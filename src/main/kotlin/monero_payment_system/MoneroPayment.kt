package monero_payment_system

import Main.Companion.errorHappened
import Main.Companion.executableParent
import Main.Companion.getIpLocation
import Main.Companion.isEmailValid
import Main.Companion.json
import Main.Companion.sendEmail
import Main.Companion.startArgs
import io.ktor.application.call
import io.ktor.features.origin
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.request.receiveParameters
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.util.encodeBase64
import io.ktor.websocket.webSocket
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.consumeEach
import org.json.JSONObject
import qrcode.Qrcode
import tumine.*
import utils.VerifyRecaptcha
import java.io.File
import java.io.FileInputStream
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.util.*
import java.net.InetAddress
import java.net.DatagramSocket



fun usdToXmr(usd: Int): BigDecimal {
    val respMap = MoneroWalletRpc.MAPPER.readTree(getTextFromUrl("https://api.coinmarketcap.com/v1/ticker/monero/?convert=usd"))
    val price = respMap.path(0)?.path("price_usd")?.asText()
    val xmrToUsd: BigDecimal = BigDecimal(price ?: "6000")
    val usdToXmr = BigDecimal(1).divide(xmrToUsd, 12, RoundingMode.HALF_UP)
    System.out.println("usd to xmr: $usdToXmr")
    return BigDecimal(usd) * usdToXmr
}

fun Route.moneroPaymentRoute() {
    //generate my qrcode for purchase + instructions for mobile users
    post("/qrcode") {
        try {
            //it's called using ajax form post
            val post = call.receiveParameters()
            val packageName = post["pkg"]
            val email = post["email"]
            val product = post["product"]

            val uniqueDeviceID = post["uniqueDeviceID"]

            val name = post["name"]
            val telephone = post["telephone"]
            val location = post["location"]
            var ip = post["ip"]

            val recaptcha = post["g-recaptcha-response"] ?: ""

            val isHowIsLifePro = product == "how_is_life_pro"

            if (!isHowIsLifePro && startArgs.contains("-stagenet")) {
                call.respondText("Sorry, I am testing something right now, you can't buy other products.")
                return@post
            }

            if (isHowIsLifePro && uniqueDeviceID == null) {
                call.respondText("ERROR")
                return@post
            }

            var locationDataIp: LocationData? = null
            if (ip != null) {
                locationDataIp = getIpLocation(ip)
            }
            val locationData = if (locationDataIp?.city != null && locationDataIp.country != null)
                locationDataIp
            else
                getIpLocation(call.request.origin.remoteHost)

            /*cool stuff: this is delivered twice:
            -from the form submit
            -from a websocket connection
    */
            val custId = post["custId"]


            //get or generate tumine.MoneroWallet, it's saved statically
            val wallet = getWallet(startArgs.contains("-stagenet")) ?: throw WalletWentShitException()

            //get integrated address, you actually need only the paymentID
            val integratedAddress = try {
                wallet.getIntegratedAddress("")
            } catch (ex: Exception) {
                errorHappened(ex)
                null
            }

            if (!isHowIsLifePro) {
                if (!VerifyRecaptcha.verify(recaptcha)) {
                    call.respondText("Please tick the recaptcha!\nThanks", status = HttpStatusCode.BadRequest)
                    return@post
                }
            }

            //smt is fucked make the user refresh
            if (integratedAddress == null || custId == null || product == null) {
                call.respondText(
                        if (!isHowIsLifePro) "<a href=\"${call.request.origin.scheme + "://" + call.request.origin.host + ":" + call.request.origin.port}/payment.html\">Error, try again</a"
                        else "Error occurred, try again later!", contentType = ContentType.parse("text/plain"), status = HttpStatusCode.InternalServerError)
                return@post
            }
            val usdAmount = databaseTUmine!!.getItemPrice(product)

            System.out.println("email: $email pkg: $packageName amount: $usdAmount")

            //convert usd to xmr using online api - can't cache it because of market volatility
            val xmrAmount = usdToXmr(usdAmount)

            //the shit (genius) xmr api wants integers which are made by multiplying a float by @tumine.getXmrDecToInt = 1 with 12 zeroes
            val xmrApiAmount = (xmrAmount * xmrDecToInt).toBigInteger()

            val qrCodeUri: String
            val qrCodeMoneroUri: MoneroUri

            //all cool, generate an payment uri and send it to monero to check
            qrCodeMoneroUri = MoneroUri(wallet.standardAddress?.standardAddress, xmrApiAmount, integratedAddress.paymentId, "", "")
            qrCodeUri = wallet.toUri(qrCodeMoneroUri).toString()


            val qrCode = Qrcode.genQRCode(qrCodeUri, FileInputStream(File(executableParent.absolutePath, "web/img/bitcoin-8bit.png")), 800)
            val base64Image =  Base64.getEncoder().encodeToString(qrCode)
            val mobilePayment = "For the mobile users:<br>address: " + qrCodeMoneroUri.address + "<br>amount (without the fee): " + xmrAmount + "<br>paymentId: " + qrCodeMoneroUri.paymentId

            if (!isHowIsLifePro) {
                if (!qrCode.isEmpty()) {
                    //respond the needed html and the page will replace the form with this
                    call.respondText(
                            """
                    <img alt="Payment QR Code" src="data:image/png;base64,$base64Image" />

                    <p style="text-align: center;">$mobilePayment</p>
                    """
                    )
                } else {
                    //respond the needed html and the page will replace the form with this
                    call.respondText(
                            """
                    <p style="text-align: center;">QR Code wasn't generated correctly!</p>

                    <p style="text-align: center;">$mobilePayment</p>
                    """
                    )
                }
            } else {
                val outputData = JSONObject()
                if (!qrCode.isEmpty()) {
                    outputData.append("image", base64Image)
                }
                outputData.append("paymentAddress", qrCodeMoneroUri.address)
                outputData.append("amount", xmrAmount)
                outputData.append("paymentId", qrCodeMoneroUri.paymentId)
                outputData.append("moneroUri", wallet.toUri(qrCodeMoneroUri).toString())
                call.respondText(outputData.toString(), contentType = ContentType.parse(json))
            }
            val waitingData = WaitingData(packageName, email, name, telephone, product, locationData, location, custId, false, xmrApiAmount, System.currentTimeMillis(), uniqueDeviceID)
            unlockPackage[integratedAddress.paymentId] = waitingData
            System.out.println(waitingData.toString())
        } catch (exc: Throwable) {
            errorHappened(exc, call)
        }
    }
    webSocket("/paymentConnector") {
        //manage those goddamn sockets
        var custId = ""
        try {
            incoming.consumeEach { frame ->
                if (frame is Frame.Text) {
                    try {
                        val text = frame.readText()
                        //you get the same custID as from the form
                        if (text.startsWith("id:")) {
                            custId = text.substring(3)
                            System.out.println("custID: $custId")
                            connectedWebsocketUsers[custId] = this
                        }
                        if (text.equals("bye", ignoreCase = true)) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                        }
                    } catch (exc: Throwable) {
                        errorHappened(exc, call)
                    }
                }
            }
        } catch (exc: Throwable) {
            errorHappened(exc)
        } finally {
            connectedWebsocketUsers.remove(custId)
            Thread(Runnable {
                try {
                    Thread.sleep(30000)
                    var foundUnlockPKGData: WaitingData? = null
                    var foundUnlockPKGKey: String? = null
                    for (unlockPKGDataKey in unlockPackage.keys) {
                        val unlockPKGData = unlockPackage[unlockPKGDataKey]
                        if (unlockPKGData != null && !unlockPKGData.passed1Stage && unlockPKGData.custId == custId) {
                            foundUnlockPKGData = unlockPKGData
                            foundUnlockPKGKey = unlockPKGDataKey
                        }
                    }
                    if (foundUnlockPKGData != null && foundUnlockPKGKey != null) {
                        if (isEmailValid(foundUnlockPKGData.email)) {
                            GlobalScope.async {
                                sendEmail(foundUnlockPKGData?.email, "You left the page!", "You left the page without sending any funds!\nIf no money are sent in the next 15 minutes the generated payment information WILL BE DELETED and if you have saved the generated QRCode, it won't work anymore!")
                            }
                            println("Waiting for this motherfucker: $custId for 15 minutes to do smt!")
                            Thread.sleep(15 * 60 * 1000)
                            println("Waited for the motherfucker: $custId 15 minutes!")
                            foundUnlockPKGData = unlockPackage[foundUnlockPKGKey]
                            if (foundUnlockPKGData != null) {
                                if (foundUnlockPKGData.passed1Stage) {
                                    println("All is ok with $custId")
                                } else {
                                    println("Fuck this guy $custId")
                                    GlobalScope.async {
                                        sendEmail(foundUnlockPKGData.email, "You left the page and the generated QRCode is now invalidated!", "The generated payment info with paymentID: $foundUnlockPKGKey is deleted from our system!\nTry again without closing the page or without paying!")
                                    }
                                    unlockPackage.remove(foundUnlockPKGKey)
                                }
                            } else {
                                unlockPackage.remove(foundUnlockPKGKey)
                            }
                        } else {
                            unlockPackage.remove(foundUnlockPKGKey)
                        }
                    }
                } catch (exc: Throwable) {
                    GlobalScope.async {
                        errorHappened(exc)
                    }
                }
            }).start()
            System.out.println("custID: $custId disconnected!")
        }
    }
}

fun initMoneroPaymentListener() {
    //the wallet instance
    val wallet = getWallet(startArgs.contains("-stagenet")) ?: throw WalletWentShitException()

    //the incoming transaction has the same height as the 'wallet.height' and it gets filtered, -2 is a workaround
    blockchainHeight = wallet.height - 2
    val timer = Timer()
    val myTask = object : TimerTask() {
        override fun run() {
            try {
                //check for transactions every 4 secs if any purchases are pending
                if (unlockPackage.isEmpty()) return

                val transactions = wallet.getTransactions(true, false, true, true, true, unlockPackage.keys, blockchainHeight, null)

                if (transactions != null && transactions.isNotEmpty()) {
                    //the expected state of the @transactions object
                    println("not null transactions with lenght: ${transactions.size}")
                    for (transaction in transactions) {
                        val unlockPkg = unlockPackage[transaction.paymentId]
                        if (unlockPkg != null) {
                            // 'unlockPkg.passed1Stage' makes sure that we don't call mempool and pending actions again
                            if (!unlockPkg.passed1Stage && (transaction.type == MoneroTransaction.MoneroTransactionType.MEMPOOL || transaction.type == MoneroTransaction.MoneroTransactionType.PENDING)) {
                                val socket = connectedWebsocketUsers[unlockPkg.custId]
                                println("not null unlockPkg")
                                println("paid for ${unlockPkg.packageName} ${unlockPkg.price} XMR")
                                if (unlockPkg.price == transaction.payments.first().amount) {
                                    println("paid exact price")
                                    //it would be cool if the user did as expected
                                    unlockPkg.passed1Stage = true
                                } else {
                                    //but he didn't rigth?
                                    val transactionMonero = transaction.payments.first().amount.toBigDecimal().divide(xmrDecToInt, MathContext(1, RoundingMode.HALF_UP))
                                    val neededMonero = unlockPkg.price.toBigDecimal().divide(xmrDecToInt, MathContext(1, RoundingMode.HALF_UP))
                                    println("paid not exact price: ${transaction.payments.first().amount} XMR")
                                    if (neededMonero <= transactionMonero) {
                                        System.out.println("but the price is close enough so pass")
                                        //just let him go on with it if this first 3 digits after the zero match
                                        unlockPkg.passed1Stage = true
                                    } else {
                                        GlobalScope.async {
                                            sendEmail(unlockPkg.email, "Transaction failed - TUmine Monero Library",
                                                    "The amount you sent ($transactionMonero XMR) with paymentID: ${transaction.paymentId} is less than the required amount: $neededMonero!\nPlease contact me to resolve this issue!")
                                        }
                                        if (socket != null) {
                                            //tell the user he's an asshole and tell him that he needs to contact me
                                            GlobalScope.async {
                                                socket.outgoing.send(Frame.Text("Transaction's amount isn't equal(or at least roughly equal) to the amount requested! Please contact me to solve this!!!"))
                                            }
                                        } else {
                                            println("socket for custId: ${unlockPkg.custId} not found")
                                        }
                                    }
                                }
                                if (unlockPkg.passed1Stage) {
                                    //get the user's socket and tell him what's going on
                                    if (socket != null) {
                                        GlobalScope.async {
                                            socket.outgoing.send(Frame.Text("Transaction received and is processing!"))
                                        }
                                    } else {
                                        println("socket for custId: ${unlockPkg.custId} not found")
                                    }
                                    GlobalScope.async {
                                        sendEmail(unlockPkg.email, "Transaction is processing - TUmine Monero Library", "The amount you sent is sufficient and your transaction is being processed by other miners!")
                                    }
                                    if (unlockPkg.product == "how_is_life_pro") {
                                        addUnlockPkgToDB(unlockPkg)
                                    }
                                    unlockPackage[transaction.paymentId] = unlockPkg
                                } else {
                                    unlockPackage.remove(transaction.paymentId)
                                }
                                println(transaction.toString())
                            } else if (transaction.type == MoneroTransaction.MoneroTransactionType.INCOMING) {
                                val socket = connectedWebsocketUsers[unlockPkg.custId]
                                if (socket != null) {
                                    GlobalScope.async {
                                        socket.outgoing.send(Frame.Text("Transaction completed!"))
                                    }
                                } else {
                                    println("socket for custId: ${unlockPkg.custId} not found")
                                }

                                if (transaction.note.isNullOrEmpty()) {
                                    transaction.note = null
                                }
                                if (unlockPkg.packageName != null) {
                                    databaseTUmine!!.addPKGtoDB(unlockPkg.packageName)
                                }

                                if (unlockPkg.product != "how_is_life_pro") {
                                    addUnlockPkgToDB(unlockPkg)
                                } else {
                                    unlockPackage.remove(transaction.paymentId)
                                    return
                                }

                                unlockPackage.remove(transaction.paymentId)
                                System.out.println(transaction.toString())

                                //some things made for another product I am selling
                                proprietaryStuff(unlockPkg)
                            } else if (transaction.type == MoneroTransaction.MoneroTransactionType.FAILED) {
                                //well fuck no money for me right
                                val socket = connectedWebsocketUsers[unlockPkg.custId]
                                if (socket != null) {
                                    GlobalScope.async {
                                        socket.outgoing.send(Frame.Text("Transaction failed, sorry!"))
                                    }
                                } else {
                                    println("socket for custId: ${unlockPkg.custId} not found")
                                }
                                GlobalScope.async {
                                    sendEmail(unlockPkg.email, "Transaction failed - TUmine Monero Library", "The transaction failed, probably no monero was sent to me.\nTry again or if the monero is already successfully sent contact me!")
                                }
                                unlockPackage.remove(transaction.paymentId)
                                println(transaction.toString())
                            }
                        }
                    }
                } else {
                    //println("${SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Date())}, still no transactions")
                }
            } catch (exc: Throwable) {
                GlobalScope.async {
                    errorHappened(exc)
                }
            }
        }

        private fun addUnlockPkgToDB(unlockPkg: WaitingData) {
            var city: String? = null
            var country: String? = null
            if (unlockPkg.location != null) {
                val spli = unlockPkg.location.replace(" ", "").split(",")
                city = spli[0]
                if (spli.size > 1) {
                    country = spli[1]
                }
            }

            databaseTUmine!!.addBuyer(unlockPkg.email,
                    unlockPkg.product,
                    unlockPkg.name,
                    unlockPkg.telephone,
                    unlockPkg.locationData.ip,
                    unlockPkg.locationData.country,
                    unlockPkg.locationData.city,
                    unlockPkg.locationData.region,
                    unlockPkg.locationData.longLat,
                    country,
                    city,
                    unlockPkg.uniqueDeviceID)

            GlobalScope.async {
                //notify me
                sendHappyPic(unlockPkg.packageName, unlockPkg.product, unlockPkg.price.toBigDecimal().divide(xmrDecToInt, MathContext(4, RoundingMode.HALF_UP)))
            }
        }
    }

    println("tumine.getBlockchainHeight: $blockchainHeight")

    //start the transaction checker
    timer.schedule(myTask, 2000, 4000)
}