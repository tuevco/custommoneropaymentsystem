package monero_payment_system

class WalletWentShitException : Exception("Something is utterly wrong with the Monero RPC Server, check if tmux with name 'monero-wallet-cli' or 'monero-wallet-rpc' is created!")
