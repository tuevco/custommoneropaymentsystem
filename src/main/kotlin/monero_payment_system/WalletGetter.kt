package monero_payment_system

import Main
import tumine.MoneroWalletRpc.MAPPER
import Passwords
import tumine.MoneroWalletRpc
import java.io.IOException
import java.lang.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.net.URL
import java.util.concurrent.TimeUnit


var REMOTE_LOGIN = false

private const val DOMAIN = "localhost"
private const val PORT = 18082
private const val USERNAME = Passwords.walletUser
private const val PASSWORD = Passwords.walletPassword
private val WALLET_FILE_STAGENET = "/${if (System.getProperty("os.name") == "Mac OS X") Passwords.macUser else Passwords.linuxUser }/Monero/wallets/stagenet/stagenet"
private val WALLET_FILE_MAINNET = "/${if (System.getProperty("os.name") == "Mac OS X") Passwords.macUser else Passwords.linuxUser }/Monero/wallets/main/main"
private const val DAEMON_ADDRESS_STAGENET = "stagenet.monerujo.io"
private const val DAEMON_PORT_STAGENET = "38081"
private const val DAEMON_ADDRESS_MAINNET = "loxone.wahl-co.de"
private const val DAEMON_PORT_MAINNET = "18089"

private var wallet: MoneroWalletRpc? = null

fun getWallet(STAGENET: Boolean): MoneroWalletRpc? {
    println(System.getProperty("os.name"))
    if (wallet == null) {
        try {
            wallet = if (System.getProperty("os.name") != "Mac OS X") MoneroWalletRpc(DOMAIN, PORT, USERNAME, PASSWORD) else MoneroWalletRpc("192.168.100.200", PORT, USERNAME, PASSWORD)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    if (!continueRunning()) {
        //val runCLI = "${if (System.getProperty("os.name") != "Mac OS X") "/usr/bin/" else ""}tmux new-session -d -s monero-wallet-cli ${if (System.getProperty("os.name") != "Mac OS X") "/${Passwords.linuxUser}/${Passwords.moneroDaemonVersion}/" else ""}monero-wallet-cli --daemon-host ${if (STAGENET) DAEMON_ADDRESS_STAGENET else DAEMON_ADDRESS_MAINNET} --daemon-port ${if (STAGENET) DAEMON_PORT_STAGENET else DAEMON_PORT_MAINNET} --wallet-file ${if (STAGENET) WALLET_FILE_STAGENET else WALLET_FILE_MAINNET} ${if (STAGENET) "--stagenet " else ""} --password $PASSWORD --allow-mismatched-daemon-version"
        //var p = Runtime.getRuntime().exec(runCLI)
        val runRPC = "${if (System.getProperty("os.name") != "Mac OS X") "/usr/bin/" else ""}tmux new-session -d -s monero-wallet-rpc ${if (System.getProperty("os.name") != "Mac OS X") "/${Passwords.linuxUser}/${Passwords.moneroDaemonVersion}/" else ""}monero-wallet-rpc --daemon-host ${if (STAGENET) DAEMON_ADDRESS_STAGENET else DAEMON_ADDRESS_MAINNET} --daemon-port ${if (STAGENET) DAEMON_PORT_STAGENET else DAEMON_PORT_MAINNET} --wallet-file ${if (STAGENET) WALLET_FILE_STAGENET else WALLET_FILE_MAINNET} ${if (STAGENET) "--stagenet " else ""}--rpc-bind-port $PORT --rpc-bind-ip 0.0.0.0 --confirm-external-bind --rpc-login $USERNAME:$PASSWORD --password $PASSWORD"
        //println("runCli: $runCLI")
        println("runRPC: $runRPC\n")
        Thread(Runnable {
            //p.waitFor(3, TimeUnit.SECONDS)
            //sleep(5000)
            val p = Runtime.getRuntime().exec(runRPC)
            p.waitFor(3, TimeUnit.SECONDS)
        }).start()
        System.out.print("Loading")
        Thread.sleep(8000)
        System.out.print(".")
        var loops = 1
        while (!continueRunning()) {
            System.out.print(".")
            Thread.sleep(4000)
            loops += 1
            if (loops >= 1000) {
                throw WalletWentShitException()
            }
            if (loops == 200) {
                Main.sendErrorMessage("More than 13 mins have passed and the wallet is unresponsive.")
            }
            if (loops == 500) {
                Main.sendErrorMessage("More than 33 mins have passed and the wallet is unresponsive.")
            }
        }
        System.out.println("Waller RAN")
        /*try {
            wallet = MoneroWalletRpc(DOMAIN, PORT, USERNAME, PASSWORD)
        } catch (e: Exception) {
            e.printStackTrace()
        }*/
    }
    return wallet
}

fun continueRunning(): Boolean {
    return try {
        wallet!!.height
        true
    } catch (ex: Exception) {
        false
    }
}


fun getTextFromUrl(url: String): String {
    val out:String
    out = try {
        URL(url).readText()
    }catch (e: IOException){
        e.printStackTrace()
        ""
    }

    return out
}