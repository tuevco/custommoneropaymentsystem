package how_is_life

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import monero_payment_system.REMOTE_LOGIN
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.json.JSONObject
import java.io.InputStream
import java.io.Serializable
import java.sql.Blob
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Types
import javax.sql.rowset.serial.SerialBlob

val objectMapper = jacksonObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

class LongBlobColumnType : ColumnType() {
    override fun sqlType(): String  = "LONGBLOB"

    override fun nonNullValueToString(value: Any): String = "?"

    override fun readObject(rs: ResultSet, index: Int): Any? {
        return if (TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream)
            rs.getBytes(index)?.let { SerialBlob(it) }
        else
            rs.getBlob(index)
    }

    override fun valueFromDB(value: Any): Any = when (value) {
        is Blob -> value
        is InputStream -> SerialBlob(value.readBytes())
        is ByteArray -> SerialBlob(value)
        else -> error("Unknown type for blob column :${value::class}")
    }

    override fun setParameter(stmt: PreparedStatement, index: Int, value: Any?) {
        when {
            TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream && value is InputStream ->
                stmt.setBinaryStream(index, value, value.available())
            value == null -> stmt.setNull(index, Types.LONGVARBINARY)
            else -> super.setParameter(stmt, index, value)
        }
    }

    override fun notNullValueToDB(value: Any): Any {
        return if (TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream)
            (value as? Blob)?.binaryStream ?: value
        else
            value
    }
}

/**
 * A long blob column to store a large amount of binary data.
 *
 * @sample org.jetbrains.exposed.sql.tests.shared.EntityTests.testBlobField
 *
 * @param name The column name
 */
fun Table.longblob(name: String): Column<Blob> = registerColumn(name, LongBlobColumnType())


class MediumBlobColumnType : ColumnType() {
    override fun sqlType(): String  = "MEDIUMBLOB"

    override fun nonNullValueToString(value: Any): String = "?"

    override fun readObject(rs: ResultSet, index: Int): Any? {
        return if (TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream)
            rs.getBytes(index)?.let { SerialBlob(it) }
        else
            rs.getBlob(index)
    }

    override fun valueFromDB(value: Any): Any = when (value) {
        is Blob -> value
        is InputStream -> SerialBlob(value.readBytes())
        is ByteArray -> SerialBlob(value)
        else -> error("Unknown type for blob column :${value::class}")
    }

    override fun setParameter(stmt: PreparedStatement, index: Int, value: Any?) {
        when {
            TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream && value is InputStream ->
                stmt.setBinaryStream(index, value, value.available())
            value == null -> stmt.setNull(index, Types.LONGVARBINARY)
            else -> super.setParameter(stmt, index, value)
        }
    }

    override fun notNullValueToDB(value: Any): Any {
        return if (TransactionManager.current().db.dialect.dataTypeProvider.blobAsStream)
            (value as? Blob)?.binaryStream ?: value
        else
            value
    }
}

/**
 * A long blob column to store a large amount of binary data.
 *
 * @sample org.jetbrains.exposed.sql.tests.shared.EntityTests.testBlobField
 *
 * @param name The column name
 */
fun Table.mediumblob(name: String): Column<Blob> = registerColumn(name, MediumBlobColumnType())

class DatabaseConnectHowIsLife(testEnvironment: Boolean = System.getProperty("os.name") == "Mac OS X") {

    enum class RegistrationType {
        Password, // 0
        Facebook, // 1
        Google,   // 2
        Kakao,    // 3
        Line,     // 4
        WeChat,   // 5
        Invalid   // 6
    }

    enum class Gender {
        Unspecified,             // 0
        Cisgender_Straight,      // 1
        Transsexual_Transgender, // 2
        Gender_NonConforming,    // 3
        None_Gender,             // 4
        Non_Binary,              // 5
        Neutrois,                // 6
        Genderfluid_Genderqueer, // 7
        Demigender,              // 8
        Agender,                 // 9
        Intergender,             // 10
        Intersex,                // 11
        Pangender,               // 12
        Poligender,              // 13
        Omnigender,              // 14
        Bigender,                // 15
        Androgyne,               // 16
        Androgyny,               // 17
        Third_Gender,            // 18
        Trigender,               // 19
    }

    enum class Sex {
        Male,       // 0
        Female,     // 1
        MtF,        // 2
        FtM,        // 3
        Unspecified // 4
    }

    data class GenderOptions(var want_to_transition: Boolean = false,
                             var transitioning: Boolean = false,
                             var has_transitioned: Boolean = false,
                             var leaning_to_male: Boolean = false,
                             var leaning_to_female: Boolean = false,
                             var notes: String = "") : Serializable {
        override fun toString(): String {
            return objectMapper.writeValueAsString(this)
        }

        companion object {
            fun fromString(data: String?): GenderOptions? {
                if (data == null) {
                    return null
                }
                return objectMapper.readValue(data, GenderOptions::class.javaObjectType)
            }
        }

        fun createMakingSenseJson(): JSONObject {
            val outJson = JSONObject();
            if (want_to_transition) {
                outJson.put("want_to_transition", true)
            }
            if (transitioning) {
                outJson.put("transitioning", true)
            }
            if (has_transitioned) {
                outJson.put("has_transitioned", true)
            }
            if (leaning_to_male) {
                outJson.put("leaning_to_male", true)
            }
            if (leaning_to_female) {
                outJson.put("leaning_to_female", true)
            }
            if (notes.isNotBlank()) {
                outJson.put("notes", notes)
            }
            return outJson
        }

        fun nullTransitionStatus() {
            want_to_transition = false
            transitioning = false
            has_transitioned = false
        }

        fun nullDemiStatus() {
            leaning_to_male = false
            leaning_to_female = false
        }
    }

    enum class JobStressLevel {
        ExtremelyStressful, // 0
        VeryStressful,      // 1
        Stressful,          // 2
        NotTooStressful,    // 3
        NotStressful,       // 4
        Relaxing            // 5
    }

    enum class Language {
        English,  // 0
        German,  // 1
        Chinese,  // 2
        Czech,  // 3
        Dutch,  // 4
        French,  // 5
        Italian,  // 6
        Japanese,  // 7
        Korean,  // 8
        Polish,  // 9
        Russian,  // 10
        Spanish,  // 11
        Arabic,  // 12
        Bulgarian,  // 13
        Catalan,  // 14
        Croatian,  // 15
        Danish,  // 16
        Finnish,  // 17
        Greek,  // 18
        Hebrew,  // 19
        Hindi,  // 20
        Hungarian,  // 21
        Indonesian,  // 22
        Latvian,  // 23
        Lithuanian,  // 24
        Norwegian,  // 25
        Portuguese,  // 26
        Romanian,  // 27
        Serbian,  // 28
        Slovak,  // 29
        Slovenian,  // 30
        Swedish,  // 31
        Tagalog,  // 32
        Thai,  // 33
        Turkish,  // 34
        Ukrainian,  // 35
        Vietnamese,  // 36
    }

    data class BasicDailyAppUsageStatistic(var pkg_name: String,
                                           var app_name: String,
                                           var usage_minutes: Long,
                                           var days_recorded: Int) : Serializable {
        override fun toString(): String {
            val string = objectMapper.writeValueAsString(this)
            println("Class: ${javaClass.simpleName} Value: $string")
            return string
        }
    }

    class AppUsageStatisticsDaily: ArrayList<BasicDailyAppUsageStatistic>() {
        override fun toString(): String {
            val string = objectMapper.writeValueAsString(this)
            println("Class: ${javaClass.simpleName} Value: $string")
            return string
        }

        companion object {
            fun fromString(data: String?): AppUsageStatisticsDaily? {
                if (data == null) {
                    return null
                }
                return objectMapper.readValue(data, AppUsageStatisticsDaily::class.javaObjectType)
            }
        }
    }

    //registered users
    object Users : Table("Users") {
        val id = long("id").autoIncrement().primaryKey()
        val username = varchar("username", length = 1024)
        val email = varchar("email", length = 255)
        val password = varchar("password", length = 2048).nullable()
        // 500px
        val profile_pic = mediumblob("profile_pic").nullable()
        // 150px
        val small_profile_pic = mediumblob("small_profile_pic").nullable()
        val gender = enumeration("gender", Gender::class).nullable()
        val spoken_languages = varchar("spoken_languages", length = 256).nullable()
        val sex = enumeration("sex", Sex::class).nullable()
        val gender_options = text("genderOptions").nullable()
        val summary = varchar("summary", length = 640).nullable()
        val resume = text("resume").nullable()
        val age = integer("age").nullable()
        val job = varchar("job", length = 2048).nullable()
        val job_stress_level = enumeration("job_stress_level", JobStressLevel::class).nullable()
        val app_usage_statistics = text("app_usage_statistics").nullable()
        val external_communications = varchar("external_communications", length = 2048).nullable()
        val login_id_3rd_party = varchar("login_id_3rd_party", length = 2048)
        val can_be_found_by_therapists = bool("can_be_found_by_therapists").default(false)
        val fcm_id = varchar("fcm_id", length = 1024).nullable()
        val registration_type = enumeration("registration_type", RegistrationType::class)
        val date_of_registration = long("date_of_registration")
    }

    object UserLogins : Table("UserLogins") {
        val id = long("id").primaryKey().autoIncrement()
        val user_id = long("user_id")
        val access_token = varchar("access_token", length = 255)
        val device = varchar("device", length = 1024)
        val date_time = long("date_time")
        val registration_type = enumeration("registration_type", RegistrationType::class)
        val ip_address = varchar("ip_address", length = 255)
    }

    object UserAccessTokens : Table("UserAccessTokens") {
        val id = long("id").primaryKey().autoIncrement()
        val user_id = long("user_id")
        val access_token = varchar("access_token", length = 255)
        val last_used = long("last_used")
        val valid = bool("valid").default(true)
    }

    enum class PermissionStatus {
        NotGranted, // 0
        Requested,  // 1
        Granted     // 2
    }

    enum class UserToTherapistPermissions {
        Email,                // 0
        ProfilePicture,       // 1
        SexAndGender,         // 2
        Resume,               // 3
        JobStressLevel,       // 4
        AppUsageStatistics,   // 5
        WorkoutStatistics,    // 6
        ExternalCommunication // 7
    }

    object TherapistUserPermissions : Table("TherapistUserPermissions") {
        val id = long("id").primaryKey().autoIncrement()
        val user_id = long("user_id")
        val therapist_id = long("therapist_id")

        val notes = text("notes").nullable()

        val email = enumeration("email", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val profile_picture = enumeration("profile_picture", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val sex_and_gender = enumeration("sex_and_gender", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val resume = enumeration("resume", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val job_stress_level = enumeration("job_stress_level", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val external_communication = enumeration("external_communication", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val app_usage_statistics = enumeration("app_usage_statistics", PermissionStatus::class).default(PermissionStatus.NotGranted)
        val workout_statistics = enumeration("workout_statistics", PermissionStatus::class).default(PermissionStatus.NotGranted)

        val is_user_therapist = bool("is_user_therapist").default(false)
    }

    //registered users
    object Therapists : Table("Therapists") {
        val id = long("id").autoIncrement().primaryKey()

        // TODO: Revoking takes care of these two
        val last_user_id = long("last_user_id").nullable()
        // TODO

        val username = varchar("username", length = 1024)
        val email = varchar("email", length = 255)
        val spoken_languages = varchar("spoken_languages", length = 256).nullable()
        val password = varchar("password", length = 2048).nullable()
        // 500px
        val profile_pic = mediumblob("profile_pic").nullable()
        // 150px
        val small_profile_pic = mediumblob("small_profile_pic").nullable()
        val gender = enumeration("gender", Gender::class).nullable()
        val sex = enumeration("sex", Sex::class).nullable()
        val gender_options = text("genderOptions").nullable()
        val external_communications = varchar("external_communications", length = 2048).nullable()
        val summary = varchar("summary", length = 160).nullable()
        val resume = text("resume").nullable()
        val age = integer("age").nullable()

        val job = varchar("job", length = 2048).nullable()
        val is_professional = bool("is_professional").default(false)

        val is_verified = bool("is_verified").default(false)

        val user_requests = text("user_requests").nullable()

        val login_id_3rd_party = varchar("login_id_3rd_party", length = 2048)
        val fcm_id = varchar("fcm_id", length = 1024).nullable()
        val registration_type = enumeration("registration_type", RegistrationType::class)
        val date_of_registration = long("date_of_registration")
    }

    object TherapistLogins : Table("TherapistLogins") {
        val id = long("id").primaryKey().autoIncrement()
        val therapist_id = long("therapist_id")
        val access_token = varchar("access_token", length = 255)
        val device = varchar("device", length = 1024)
        val date_time = long("date_time")
        val registration_type = enumeration("registration_type", RegistrationType::class)
        val ip_address = varchar("ip_address", length = 255)
    }

    object TherapistAccessTokens : Table("TherapistAccessTokens") {
        val id = long("id").primaryKey().autoIncrement()
        val therapist_id = long("therapist_id")
        val access_token = varchar("access_token", length = 255)
        val last_used = long("last_used")
        val valid = bool("valid").default(true)
    }

    //all the info a mood box can contain (associated with user email)
    object MoodBoxesInfo : Table("MoodBoxesInfo") {
        val id = long("id").primaryKey().autoIncrement()
        val user_id = long("user_id")
        val date_modified = long("date_modified")
        val day = integer("day")
        val month = integer("month")
        val year = integer("year")
        val mood = integer("mood")
        val notes = varchar("notes", length = 1024).nullable()
        val therapists_notes = varchar("therapists_notes", length = 1024).nullable()
    }

    var db: Database = if (testEnvironment) {
        Database.connect("jdbc:mysql://${if (System.getProperty("os.name") != "Mac OS X" || REMOTE_LOGIN) "localhost" else "192.168.100.200"}:3306/how_is_life_test?useUnicode=true&characterEncoding=UTF-8&useSSL=false", driver = "com.mysql.cj.jdbc.Driver", user = Passwords.mysqlUser, password = Passwords.mysqlPassword)
    } else {
        Database.connect("jdbc:mysql://${if (System.getProperty("os.name") != "Mac OS X" || REMOTE_LOGIN) "localhost" else "192.168.100.200"}:3306/how_is_life?useUnicode=true&characterEncoding=UTF-8&useSSL=false", driver = "com.mysql.cj.jdbc.Driver", user = Passwords.mysqlUser, password = Passwords.mysqlPassword)
    }

    init {
        transaction(db) {
            // User stuff
            Users.createStatement().forEach {
                exec(it)
            }
            UserAccessTokens.createStatement().forEach {
                exec(it)
            }
            MoodBoxesInfo.createStatement().forEach {
                exec(it)
            }
            UserLogins.createStatement().forEach {
                exec(it)
            }

            TherapistUserPermissions.createStatement().forEach {
                exec(it)
            }

            // Therapist stuff
            Therapists.createStatement().forEach {
                exec(it)
            }
            TherapistLogins.createStatement().forEach {
                exec(it)
            }
            TherapistAccessTokens.createStatement().forEach {
                exec(it)
            }
        }
    }
}