@file:Suppress("DeferredResultUnused", "RemoveRedundantQualifierName")

package how_is_life

import Main.Companion.errorHappened
import Main.Companion.executableParent
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.apache.ApacheHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import how_is_life.DatabaseConnectHowIsLife.RegistrationType.*
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.features.origin
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.*
import io.ktor.request.receive
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.response.respondFile
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.websocket.webSocket
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateStatement
import org.jetbrains.exposed.sql.transactions.transaction
import org.json.JSONArray
import org.json.JSONObject
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.net.URL
import java.security.SecureRandom
import java.sql.Blob
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection
import javax.sql.rowset.serial.SerialBlob
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


var db = DatabaseConnectHowIsLife()
private val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
private val jacksonFactory = JacksonFactory()
private val transport: HttpTransport = ApacheHttpTransport()

/*  Google Stuff    */
val verifier = GoogleIdTokenVerifier.Builder(transport, jacksonFactory)
        .setAudience(listOf(
                "879841686674-tinb46c29ed3j6ch25jcu1jcietqjj6v.apps.googleusercontent.com",
                "879841686674-uacj1cicb67jed48b4tnvtjlpt1sl17n.apps.googleusercontent.com",
                "879841686674-fnqti7ldogqnub7tqpkopml26b5miqte.apps.googleusercontent.com",
                "879841686674-mujrhbsp75cc7h93lt7fnb18fkde7qjo.apps.googleusercontent.com",
                "879841686674-6j3orj3s0lg4bfui8feu6v2jloin6h13.apps.googleusercontent.com",
                "879841686674-qtfalrh2i0gncp1j4118sro94caflj7j.apps.googleusercontent.com"))
        .build()

/*  Facebook Stuff    */
//Same as appId
var clientId = "2251019741877300"
//same as appSecret
var clientSecret = "d97ecbfca26df8e1b663b0ac52d1bc82"

var appLink =
        "https://graph.facebook.com/oauth/access_token?client_id=$clientId&client_secret=$clientSecret&grant_type=client_credentials"

//                                        <userID, WebSocketConnectors>
var connectedMoodboxWebsocketUsers = HashMap<Long, MutableList<WebSocketSession>>()

//                                                      <accessToken, WebSocketConnectors>
var connectedWaitingVerificationWebsocketUsers = HashMap<String, MutableList<WebSocketSession>>()

fun initHowIsLife() {

}

/**
 * @return userID, -1 if not valid, -2 if unsigned
 */
fun verifyUserInternalAccessToken(accessToken: String): Long {
    var userID: Long = verifyInternalUserAccessTokenCareAboutValidity(accessToken)
    if (userID == -3L) {
        userID = -1
    }
    return userID
}

/**
 * @return userID, -1 if not valid, -2 if unsigned, -3 if waiting validation
 */
fun verifyInternalUserAccessTokenCareAboutValidity(accessToken: String): Long {
    var userID: Long = -1
    if (accessToken.isBlank()) {
        return userID
    }
    transaction(db.db) {
        val item = DatabaseConnectHowIsLife.UserAccessTokens.select { (DatabaseConnectHowIsLife.UserAccessTokens.access_token eq accessToken)}.firstOrNull()
        if (item != null) {
            val internalID = item[DatabaseConnectHowIsLife.UserAccessTokens.id]
            val lastUsed = item[DatabaseConnectHowIsLife.UserAccessTokens.last_used]
            if (System.currentTimeMillis() - lastUsed > (90.toLong() * 24.toLong() * 60.toLong() * 60.toLong() * 1000.toLong())) {
                userID = -2
                DatabaseConnectHowIsLife.UserAccessTokens.deleteWhere { DatabaseConnectHowIsLife.UserAccessTokens.id eq internalID }
            } else {
                if (item[DatabaseConnectHowIsLife.UserAccessTokens.valid]) {
                    userID = item[DatabaseConnectHowIsLife.UserAccessTokens.user_id]
                    DatabaseConnectHowIsLife.UserAccessTokens.update({ DatabaseConnectHowIsLife.UserAccessTokens.id eq internalID }) {
                        it[DatabaseConnectHowIsLife.UserAccessTokens.last_used] = System.currentTimeMillis()
                    }
                } else {
                    userID = -3
                }
            }
        }
    }
    return userID
}

/**
 * @return therapistID, -1 if not valid, -2 if unsigned
 */
fun verifyTherapistInternalAccessToken(accessToken: String): Long {
    var therapistID: Long = verifyInternalTherapistAccessTokenCareAboutValidity(accessToken)
    if (therapistID == -3L) {
        therapistID = -1
    }
    return therapistID
}

/**
 * @return therapistID, -1 if not valid, -2 if unsigned, -3 if waiting validation
 */
fun verifyInternalTherapistAccessTokenCareAboutValidity(accessToken: String): Long {
    var therapistID: Long = -1
    transaction(db.db) {
        val item = DatabaseConnectHowIsLife.TherapistAccessTokens.select { (DatabaseConnectHowIsLife.TherapistAccessTokens.access_token eq accessToken)}.firstOrNull()
        if (item != null) {
            val internalID = item[DatabaseConnectHowIsLife.TherapistAccessTokens.id]
            val lastUsed = item[DatabaseConnectHowIsLife.TherapistAccessTokens.last_used]
            if (System.currentTimeMillis() - lastUsed > (90.toLong() * 24.toLong() * 60.toLong() * 60.toLong() * 1000.toLong())) {
                therapistID = -2
                DatabaseConnectHowIsLife.TherapistAccessTokens.deleteWhere { DatabaseConnectHowIsLife.TherapistAccessTokens.id eq internalID }
            } else {
                if (item[DatabaseConnectHowIsLife.TherapistAccessTokens.valid]) {
                    therapistID = item[DatabaseConnectHowIsLife.TherapistAccessTokens.therapist_id]
                    DatabaseConnectHowIsLife.TherapistAccessTokens.update({ DatabaseConnectHowIsLife.TherapistAccessTokens.id eq internalID }) {
                        it[DatabaseConnectHowIsLife.TherapistAccessTokens.last_used] = System.currentTimeMillis()
                    }
                } else {
                    therapistID = -3
                }
            }
        }
    }
    return therapistID
}

fun sendChangedMoodboxesToClients(userID: Long, moodboxes: JSONArray) {
    val socketList = connectedMoodboxWebsocketUsers[userID]
    if (socketList != null) {
        if (socketList.isEmpty()) {
            println("socket for $userID empty")
        } else {
            println("socket for $userID is ${socketList.size} long")
        }
        if (moodboxes.length() == 0) {
            println("moodboxes is empty")
            return;
        }
        for (socket in socketList) {
            val output = JSONObject()
            output.put("access_token", "")
            output.put("moodboxes", moodboxes)
            val toSend = output.toString();
            GlobalScope.async {
                socket.outgoing.send(Frame.Text(toSend))
                println("sent to socket: $toSend")
            }
        }
    } else {
        println("socket for $userID null")
    }
}

/**
 * @return JSON to be parsed by the Flutter app
 */
fun getAllEditsSinceTimemillis(millis: Long, userID: Long): JSONArray {
    val savedBoxes = JSONArray()
    transaction(db.db) {
        DatabaseConnectHowIsLife.MoodBoxesInfo.select {
            (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID) and
                    (DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified greater millis)
        }.forEach {
            val moodBox = JSONObject()
            moodBox.put("day", it[DatabaseConnectHowIsLife.MoodBoxesInfo.day])
            moodBox.put("month", it[DatabaseConnectHowIsLife.MoodBoxesInfo.month])
            moodBox.put("year", it[DatabaseConnectHowIsLife.MoodBoxesInfo.year])
            moodBox.put("mood", it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood])
            moodBox.put("note", it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes])
            moodBox.put("edited_time", it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified])
            savedBoxes.put(moodBox)
        }
    }
    return savedBoxes
}

// for get profile
suspend fun getTherapistRegisteredUsers(accessToken: String? = null, call: ApplicationCall? = null, therapistID: Long? = null, userID: Long? = null): JSONObject? {
    val therapistIDVerified: Long
    if (accessToken != null) {
        therapistIDVerified = verifyTherapistInternalAccessToken(accessToken)
        // unauthenticated after 90 day inactivity
        if (therapistIDVerified == -2L) {
            call?.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return null
        }
        if (therapistIDVerified == -1L) {
            call?.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return null
        }
    } else if (therapistID != null) {
        therapistIDVerified = therapistID
    } else {
        return null
    }

    /*
        if something exists but is "-1" or "" -> allowed not present

        jsonOUT:
            `userID`: [user_id, notes, `data`]
     */
    val jsonOut = JSONObject()
    var users = 0
    var userJSON = JSONObject()
    transaction(db.db) {
        val statement = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
            DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id eq therapistIDVerified
        }
        if (userID != null) {
            statement.andWhere { DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq userID }
        }
        statement.forEach {
            println("statement called foreach")
            userJSON = JSONObject()
            val currentUserID = it[DatabaseConnectHowIsLife.TherapistUserPermissions.user_id]
            userJSON.put("user_id", currentUserID)
            userJSON.put("notes", it[DatabaseConnectHowIsLife.TherapistUserPermissions.notes])

            val userInfo: ResultRow? = DatabaseConnectHowIsLife.Users.select {
                DatabaseConnectHowIsLife.Users.id eq currentUserID
            }.firstOrNull()
            if (userInfo != null) {
                userJSON.put("username", userInfo[DatabaseConnectHowIsLife.Users.username])
                userJSON.put("summary", userInfo[DatabaseConnectHowIsLife.Users.summary])
                userJSON.put("age", userInfo[DatabaseConnectHowIsLife.Users.age])
                userJSON.put("job", userInfo[DatabaseConnectHowIsLife.Users.job])

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.external_communication] == DatabaseConnectHowIsLife.PermissionStatus.Granted)
                    userJSON.put("external_communications", userInfo[DatabaseConnectHowIsLife.Users.external_communications])

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.email] == DatabaseConnectHowIsLife.PermissionStatus.Granted)
                    userJSON.put("email", userInfo[DatabaseConnectHowIsLife.Users.email])

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.profile_picture] == DatabaseConnectHowIsLife.PermissionStatus.Granted) {
                    val profilePicBlob = userInfo[DatabaseConnectHowIsLife.Users.profile_pic]
                    userJSON.put("profile_pic", profilePicBlob != null)
                    profilePicBlob?.free()
                }

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.sex_and_gender] == DatabaseConnectHowIsLife.PermissionStatus.Granted) {
                    userJSON.put("sex", userInfo[DatabaseConnectHowIsLife.Users.sex]?.ordinal)

                    userJSON.put("gender", userInfo[DatabaseConnectHowIsLife.Users.gender]?.ordinal)
                    val options = userInfo[DatabaseConnectHowIsLife.Users.gender_options]
                    userJSON.put("gender_options", DatabaseConnectHowIsLife.GenderOptions.fromString(options)?.createMakingSenseJson())
                }

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.resume] == DatabaseConnectHowIsLife.PermissionStatus.Granted)
                    userJSON.put("resume", userInfo[DatabaseConnectHowIsLife.Users.resume])

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.job_stress_level] == DatabaseConnectHowIsLife.PermissionStatus.Granted)
                    userJSON.put("job_stress_level", userInfo[DatabaseConnectHowIsLife.Users.job_stress_level]?.ordinal)

                if (it[DatabaseConnectHowIsLife.TherapistUserPermissions.app_usage_statistics] == DatabaseConnectHowIsLife.PermissionStatus.Granted)
                    userJSON.put("app_usage_statistics", userInfo[DatabaseConnectHowIsLife.Users.app_usage_statistics]?.toString())
            } else {
                userJSON.put("error", "no_data")
            }

            jsonOut.put(currentUserID.toString(), userJSON)
            users++
        }
        // no previous connection `therapist - user` get basic data
        if (users == 0 && userID != null) {
            println("statement called foreach")
            userJSON = JSONObject()
            val currentUserID = userID
            userJSON.put("user_id", currentUserID)

            val userInfo: ResultRow? = DatabaseConnectHowIsLife.Users.select {
                DatabaseConnectHowIsLife.Users.id eq currentUserID
            }.firstOrNull()
            if (userInfo != null) {
                userJSON.put("username", userInfo[DatabaseConnectHowIsLife.Users.username])
                userJSON.put("summary", userInfo[DatabaseConnectHowIsLife.Users.summary])
                userJSON.put("age", userInfo[DatabaseConnectHowIsLife.Users.age])
                userJSON.put("job", userInfo[DatabaseConnectHowIsLife.Users.job])
            } else {
                userJSON.put("error", "no_data")
            }

            jsonOut.put(currentUserID.toString(), userJSON)
            users++
        }
    }
    if (users == 1) {
        return userJSON
    }
    return jsonOut
}

fun getPermissionRow(userToTherapistPermissions: DatabaseConnectHowIsLife.UserToTherapistPermissions): Column<DatabaseConnectHowIsLife.PermissionStatus> {
    return when(userToTherapistPermissions) {
        DatabaseConnectHowIsLife.UserToTherapistPermissions.Email -> DatabaseConnectHowIsLife.TherapistUserPermissions.email
        DatabaseConnectHowIsLife.UserToTherapistPermissions.ProfilePicture -> DatabaseConnectHowIsLife.TherapistUserPermissions.profile_picture
        DatabaseConnectHowIsLife.UserToTherapistPermissions.SexAndGender -> DatabaseConnectHowIsLife.TherapistUserPermissions.sex_and_gender
        DatabaseConnectHowIsLife.UserToTherapistPermissions.Resume -> DatabaseConnectHowIsLife.TherapistUserPermissions.resume
        DatabaseConnectHowIsLife.UserToTherapistPermissions.JobStressLevel -> DatabaseConnectHowIsLife.TherapistUserPermissions.job_stress_level
        DatabaseConnectHowIsLife.UserToTherapistPermissions.AppUsageStatistics -> DatabaseConnectHowIsLife.TherapistUserPermissions.app_usage_statistics
        DatabaseConnectHowIsLife.UserToTherapistPermissions.WorkoutStatistics -> DatabaseConnectHowIsLife.TherapistUserPermissions.workout_statistics
        DatabaseConnectHowIsLife.UserToTherapistPermissions.ExternalCommunication -> DatabaseConnectHowIsLife.TherapistUserPermissions.external_communication
    }
}

@Suppress("RemoveRedundantQualifierName")
fun Route.howIsLifeRoutes() {
    //Therapists



    //Users
    get("how-is-life") {
        call.respondText("How Is Life")
    }
    get("how-is-life/privacy_policy") {
        call.respondFile(File(executableParent.absolutePath, "web"), "privacy_policy-how_is_life.html")
    }
    post("how-is-life/register_howdy") {
        try {
            val regInfo = call.receive<RegisterInfo>()
            println("register: $regInfo")
            val registrationType = getRegistrationTypeFromString(regInfo.signInMethod)
            val internalUserID = when (registrationType) {
                Google -> handleRegisterGoogle(regInfo, registrationType, call, regInfo.isTherapist)
                Facebook -> handleRegisterFacebook(regInfo, registrationType, call, regInfo.isTherapist)
                Password -> handleRegisterPassword(regInfo, call, regInfo.isTherapist)
                else -> Pair(-1L, "")
            }
            var userData: JSONObject? = null
            if (regInfo.isTherapist) {
                userData = getTherapistRegisteredUsers(therapistID = internalUserID.first)
            }
            var savedLastUserID: Long? = null
            // for connected clients
            val changedBoxes = JSONArray()
            // for the device registering
            val savedBoxes = JSONArray()
            transaction(db.db) {
                if (!regInfo.isTherapist && regInfo.dataSoFar.isNotBlank()) {
                    val jsonOfMoodBoxes = JSONArray(regInfo.dataSoFar)
                    for (jsonObjectRaw in jsonOfMoodBoxes) {
                        val jsonObject = jsonObjectRaw as JSONObject
                        val day = jsonObject.optInt("day")
                        val month = jsonObject.optInt("month")
                        val year = jsonObject.optInt("year")
                        val mood = jsonObject.optInt("mood")
                        val note = jsonObject.optString("note")
                        val edited_time = jsonObject.optLong("edited_time")

                        val serverUsersData = DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                            (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq internalUserID.first) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.day eq day) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.month eq month) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.year eq year)
                        }.firstOrNull()

                        if (serverUsersData != null) {
                            if (serverUsersData[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] < edited_time) {
                                DatabaseConnectHowIsLife.MoodBoxesInfo.update({ DatabaseConnectHowIsLife.MoodBoxesInfo.id eq serverUsersData[DatabaseConnectHowIsLife.MoodBoxesInfo.id] }) {
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = edited_time
                                }
                                changedBoxes.put(jsonObject)
                            } // else do nothing because the data in the server is more Recent
                        } else {
                            DatabaseConnectHowIsLife.MoodBoxesInfo.insert {
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.user_id] = internalUserID.first
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = System.currentTimeMillis()
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.day] = day
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.month] = month
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.year] = year
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                            }
                            changedBoxes.put(jsonObject)
                        }
                    }
                }

                savedLastUserID = if (!regInfo.isTherapist) {
                    null
                } else {
                    DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq internalUserID.first }
                        .firstOrNull()?.get(DatabaseConnectHowIsLife.Therapists.last_user_id)
                }

                val userID = if (!regInfo.isTherapist) {
                    internalUserID.first
                } else  {
                    val key = userData?.keySet()?.firstOrNull()
                    savedLastUserID ?: (if (key != null) (userData?.getJSONObject(key)?.getLong("user_id")) else null)
                }
                if (userID != null) {
                    if (savedLastUserID == null) {
                        DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq internalUserID.first }) {
                            it[DatabaseConnectHowIsLife.Therapists.last_user_id] = userID
                        }
                    }
                    DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID)
                    }.forEach {
                        val moodBox = JSONObject()
                        moodBox.put("day", it[DatabaseConnectHowIsLife.MoodBoxesInfo.day])
                        moodBox.put("month", it[DatabaseConnectHowIsLife.MoodBoxesInfo.month])
                        moodBox.put("year", it[DatabaseConnectHowIsLife.MoodBoxesInfo.year])
                        moodBox.put("mood", it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood])
                        moodBox.put("note", it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes])
                        moodBox.put("edited_time", it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified])
                        savedBoxes.put(moodBox)
                    }
                }
            }
            val output = JSONObject()
            if (regInfo.isTherapist) {
                if (userData != null) {
                    output.put("user_ids", userData)
                }
                if (savedLastUserID != null) {
                    output.put("last_user_id", savedLastUserID!!)
                }
            }
            output.put("access_token", internalUserID.second)
            output.put("moodboxes", savedBoxes)
            call.respondText(output.toString())
            if (!regInfo.isTherapist && regInfo.dataSoFar.isNotBlank()) {
                sendChangedMoodboxesToClients(internalUserID.first, changedBoxes)
            }
        } catch (exc: Throwable) {
            exc.printStackTrace()
            //errorHappened(exc, call)
        }
    }
    post("how-is-life/gen_invalid_auth_token") {
        try {
            //TODO: Therapists
            val regInfo = call.receive<RegisterInfo>()
            println("register: $regInfo")
            val accessToken = generateAccessToken(220)
            transaction(db.db) {
                DatabaseConnectHowIsLife.UserAccessTokens.insert {
                    it[user_id] = -1
                    it[access_token] = accessToken
                    it[last_used] = System.currentTimeMillis()
                    it[valid] = false
                }
                DatabaseConnectHowIsLife.UserLogins.insert {
                    it[user_id] = -1
                    it[access_token] = accessToken
                    it[device] = regInfo.device
                    it[date_time] = System.currentTimeMillis()
                    it[registration_type] = Invalid
                    it[ip_address] = call.request.origin.remoteHost
                }
            }
            call.respondText(accessToken)
        } catch (exc: Throwable) {
            exc.printStackTrace()
            //errorHappened(exc, call)
        }
    }
    post("how-is-life/verify_invalid_auth_token") {
        try {
            //TODO: Therapists
            val verifyInfo = call.receive<VerifyInvalidToken>()
            println("verifyInfo: $verifyInfo")
            val validUserID = verifyUserInternalAccessToken(verifyInfo.validAccessToken)
            // unauthenticated after 90 day inactivity
            if (validUserID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                return@post
            }
            if (validUserID == -1L) {
                call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                return@post
            }
            transaction(db.db) {
                DatabaseConnectHowIsLife.UserAccessTokens.update({ DatabaseConnectHowIsLife.UserAccessTokens.access_token eq verifyInfo.tokenToBeValidated }) {
                    it[valid] = true
                    it[user_id] = validUserID
                }
                DatabaseConnectHowIsLife.UserLogins.update({DatabaseConnectHowIsLife.UserLogins.access_token eq verifyInfo.tokenToBeValidated}) {
                    it[user_id] = validUserID
                }
            }
            call.respondText("ok")
            val socketList = connectedWaitingVerificationWebsocketUsers[verifyInfo.tokenToBeValidated]
            if (socketList != null) {
                if (socketList.isEmpty()) {
                    println("verification socket for ${verifyInfo.tokenToBeValidated} empty")
                } else {
                    println("verification socket for ${verifyInfo.tokenToBeValidated} is ${socketList.size} long")
                }
                for (socket in socketList) {
                    val toSend = "verified"
                    GlobalScope.async {
                        socket.outgoing.send(Frame.Text(toSend))
                        println("verification sent to socket: $toSend")
                    }
                }
            } else {
                println("verification socket for ${verifyInfo.tokenToBeValidated} null")
            }
        } catch (exc: Throwable) {
            exc.printStackTrace()
            //errorHappened(exc, call)
        }
    }
    post("how-is-life/has_account") {
        try {
            val hasAccountInfo = call.receive<LoginInfoExists>()
            println("hasAccountInfo: $hasAccountInfo")
            val registrationType = getRegistrationTypeFromString(hasAccountInfo.signInMethod)
            when (registrationType) {
                Google -> {
                    var retries = 5
                    var hasError = true
                    while (retries >= 0 && hasError) {
                        retries--
                        try {
                            val idToken: GoogleIdToken? = verifier.verify(hasAccountInfo.access_token)
                            if (idToken != null) {
                                val payload = idToken.payload

                                // Print user identifier
                                val userId = payload.subject
                                println("User ID: $userId")

                                // Get profile information from payload
                                val emailGoogle = payload.email
                                val name = (payload["name"] as String?).orEmpty().ifEmpty { emailGoogle.substring(startIndex = 0, endIndex = emailGoogle.indexOf('@')) }
                                println("name: $name")


                            } else {
                                println("Invalid ID token.")
                                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                            }
                            hasError = false
                        } catch (exc: Throwable) {
                            hasError = true
                            exc.printStackTrace()
                        }
                    }
                }
            }
        } catch (exc: Throwable) {
            exc.printStackTrace()
            //errorHappened(exc, call)
        }
    }

    post("how-is-life/get_changes") {
        val requestData = JSONObject(call.receiveText())
        val dataSoFar = requestData.getString("dataSoFar")
        val accessToken = requestData.getString("access_token")
        val isTherapist = requestData.getBoolean("isTherapist")
        if (!isTherapist) {
            val userID = verifyUserInternalAccessToken(accessToken)
            // unauthenticated after 90 day inactivity
            if (userID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                return@post
            }
            if (userID == -1L) {
                call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                return@post
            }

            // for connected clients
            val changedBoxes = JSONArray()

            if (dataSoFar.isNotEmpty()) {
                val jsonOfMoodBoxes = JSONArray(dataSoFar)
                transaction(db.db) {
                    for (jsonObjectRaw in jsonOfMoodBoxes) {
                        val jsonObject = jsonObjectRaw as JSONObject
                        val day = jsonObject.optInt("day")
                        val month = jsonObject.optInt("month")
                        val year = jsonObject.optInt("year")
                        val mood = jsonObject.optInt("mood")
                        val note = jsonObject.optString("note")
                        val edited_time = jsonObject.optLong("edited_time")

                        val serverUsersData = DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                            (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.day eq day) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.month eq month) and
                                    (DatabaseConnectHowIsLife.MoodBoxesInfo.year eq year)
                        }.firstOrNull()

                        if (serverUsersData != null) {
                            if (serverUsersData[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] < edited_time) {
                                DatabaseConnectHowIsLife.MoodBoxesInfo.update({ DatabaseConnectHowIsLife.MoodBoxesInfo.id eq serverUsersData[DatabaseConnectHowIsLife.MoodBoxesInfo.id] }) {
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = edited_time
                                }
                                changedBoxes.put(jsonObject)
                            } // else do nothing because the data in the server is more Recent
                        } else {
                            DatabaseConnectHowIsLife.MoodBoxesInfo.insert {
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.user_id] = userID
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = System.currentTimeMillis()
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.day] = day
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.month] = month
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.year] = year
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                                it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                            }
                            changedBoxes.put(jsonObject)
                        }
                    }
                }
            }

            val last_time_synced = requestData.getLong("last_edit")
            val thingsPending = getAllEditsSinceTimemillis(last_time_synced, userID)
            val output = JSONObject()
            output.put("access_token", "")
            output.put("moodboxes", thingsPending)
            call.respondText(output.toString())
            sendChangedMoodboxesToClients(userID, changedBoxes)
        } else {
            val therapistID = verifyTherapistInternalAccessToken(accessToken)
            // unauthenticated after 90 day inactivity
            if (therapistID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                return@post
            }
            if (therapistID == -1L) {
                call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                return@post
            }
            val userData: JSONObject? = getTherapistRegisteredUsers(therapistID = therapistID)
            val savedBoxes = JSONArray()
            var lastUserID: Long? = null
            transaction(db.db) {
                val key = userData?.keySet()?.firstOrNull()

                val savedLastUserID = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq therapistID }
                        .firstOrNull()?.get(DatabaseConnectHowIsLife.Therapists.last_user_id)
                lastUserID = savedLastUserID ?: (if (key != null) (userData.getJSONObject(key)?.getLong("user_id")) else null)

                if (lastUserID != null) {
                    if (savedLastUserID == null) {
                        DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq therapistID }) {
                            it[DatabaseConnectHowIsLife.Therapists.last_user_id] = lastUserID!!
                        }
                    }
                    DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq lastUserID!!)
                    }.forEach {
                        val moodBox = JSONObject()
                        moodBox.put("day", it[DatabaseConnectHowIsLife.MoodBoxesInfo.day])
                        moodBox.put("month", it[DatabaseConnectHowIsLife.MoodBoxesInfo.month])
                        moodBox.put("year", it[DatabaseConnectHowIsLife.MoodBoxesInfo.year])
                        moodBox.put("mood", it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood])
                        moodBox.put("note", it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes])
                        moodBox.put("edited_time", it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified])
                        savedBoxes.put(moodBox)
                    }
                }
            }
            val output = JSONObject()
            if (userData != null) {
                output.put("user_ids", userData)
            }
            if (lastUserID != null) {
                output.put("last_user_id", lastUserID!!)
            }
            output.put("access_token", "")
            output.put("moodboxes", savedBoxes)
            call.respondText(output.toString())
        }
    }

    post("how-is-life/get_only_today") {
        val requestData = JSONObject(call.receiveText())
        val accessToken = requestData.getString("access_token")
        val userID = verifyUserInternalAccessToken(accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        val savedBoxes = JSONArray()

        val day = requestData.optInt("day")
        val month = requestData.optInt("month")
        val year = requestData.optInt("year")
        val mood = requestData.optInt("mood")
        val note = requestData.optString("note")
        val user_edit_time = requestData.optLong("user_edit_time")

        // for connected clients
        val changedBoxes = JSONArray()

        transaction(db.db) {
            var usersData = DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.day eq day) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.month eq month) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.year eq year)
            }.firstOrNull()

            if (usersData != null) {
                if (usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] <= user_edit_time) {
                    DatabaseConnectHowIsLife.MoodBoxesInfo.update({
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID) and
                                (DatabaseConnectHowIsLife.MoodBoxesInfo.day eq day) and
                                (DatabaseConnectHowIsLife.MoodBoxesInfo.month eq month) and
                                (DatabaseConnectHowIsLife.MoodBoxesInfo.year eq year)
                    }) {
                        it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                        it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                        it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = user_edit_time
                    }
                    usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                    usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                    usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = user_edit_time
                    requestData.remove("access_token")
                    changedBoxes.put(requestData)
                }
                val moodBox = JSONObject()
                moodBox.put("day", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.day])
                moodBox.put("month", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.month])
                moodBox.put("year", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.year])
                moodBox.put("mood", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.mood])
                moodBox.put("note", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.notes])
                moodBox.put("edited_time", usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified])
                savedBoxes.put(moodBox)
            }
        }
        val output = JSONObject()
        output.put("access_token", "")
        output.put("moodboxes", savedBoxes)
        call.respondText(output.toString())
        sendChangedMoodboxesToClients(userID, changedBoxes)
    }

    post("how-is-life/set_moodbox") {
        val moodboxData = JSONObject(call.receiveText())
        val accessToken = moodboxData.getString("access_token")
        val last_time_synced = moodboxData.getLong("last_edit")
        val day = moodboxData.optInt("day")
        val month = moodboxData.optInt("month")
        val year = moodboxData.optInt("year")
        val mood = moodboxData.optInt("mood")
        val note = moodboxData.optString("note")
        val edited_time = moodboxData.optLong("edited_time")

        val userID = verifyUserInternalAccessToken(accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        val thingsPending = getAllEditsSinceTimemillis(last_time_synced, userID)
        transaction(db.db) {
            val usersData = DatabaseConnectHowIsLife.MoodBoxesInfo.select {
                (DatabaseConnectHowIsLife.MoodBoxesInfo.user_id eq userID) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.day eq day) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.month eq month) and
                        (DatabaseConnectHowIsLife.MoodBoxesInfo.year eq year)
            }.firstOrNull()

            if (usersData != null) {
                DatabaseConnectHowIsLife.MoodBoxesInfo.update({ DatabaseConnectHowIsLife.MoodBoxesInfo.id eq usersData[DatabaseConnectHowIsLife.MoodBoxesInfo.id] }) {
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = edited_time
                }
            } else {
                DatabaseConnectHowIsLife.MoodBoxesInfo.insert {
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.user_id] = userID
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.date_modified] = edited_time
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.day] = day
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.month] = month
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.year] = year
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.mood] = mood
                    it[DatabaseConnectHowIsLife.MoodBoxesInfo.notes] = note
                }
            }
        }
        // for connected clients
        val changedBoxes = JSONArray()

        val moodBox = JSONObject()
        moodBox.put("day", day)
        moodBox.put("month", month)
        moodBox.put("year", year)
        moodBox.put("mood", mood)
        moodBox.put("note", note)
        moodBox.put("edited_time", edited_time)

        changedBoxes.put(moodBox)

        val output = JSONObject()
        output.put("access_token", "")
        output.put("moodboxes", thingsPending)
        call.respondText(output.toString())
        sendChangedMoodboxesToClients(userID, changedBoxes)
    }

    post("how-is-life/login_howdy") { _ ->
        try {
            val logInfo = call.receive<LoginInfo>()
            transaction(db.db) {
                val res = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.email eq logInfo.email }
                if (res.count() > 0) {
                    if (BCrypt.checkpw(logInfo.password, res.first()[DatabaseConnectHowIsLife.Users.password])) {
                        GlobalScope.async {
                            call.respondText("done")
                        }
                    } else {
                        GlobalScope.async {
                            call.respondText("pswd wrong")
                        }
                    }
                } else {
                    GlobalScope.async {
                        call.respondText("no usr")
                    }
                }
            }
        } catch (exc: Throwable) {
            exc.printStackTrace()
            //errorHappened(exc, call)
        }
    }
    post("how-is-life/get_data") {
        val tokenInfo = call.receive<GetData>()
        if (tokenInfo.isTherapist) {
            val therapistID = verifyTherapistInternalAccessToken(tokenInfo.accessToken)
            // unauthenticated after 90 day inactivity
            if (therapistID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            }
            if (therapistID == -1L) {
                call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
                return@post
            }

            if (tokenInfo.userID != null) {
                val userData: JSONObject? = getTherapistRegisteredUsers(therapistID = therapistID, userID = tokenInfo.userID)
                if (userData != null) {
                    call.respond(userData.toString())
                } else {
                    call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
                }
                return@post
            }

            val therapistData: TherapistData? = getTherapistData(therapistID, false)
            if (therapistData != null) {
                call.respond(therapistData)
            } else {
                call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
            }
        } else {
            val userID = verifyUserInternalAccessToken(tokenInfo.accessToken)
            println("userID: $userID")
            // unauthenticated after 90 day inactivity
            if (userID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                return@post
            }
            if (userID == -1L) {
                call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
                return@post
            }
            if (tokenInfo.therapistID != null) {
                val therapistData: TherapistData? = getTherapistData(tokenInfo.therapistID, true)
                if (therapistData != null) {
                    call.respond(therapistData)
                } else {
                    call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
                }
                return@post
            }
            val userData: UserData? = getUserData(userID)
            if (userData != null) {
                call.respond(userData)
            } else {
                call.respondText("Error occurred!\nTry again later.", status = HttpStatusCode.InternalServerError)
            }
        }
    }

    post("how-is-life/log_out") {
        val tokenInfo = call.receive<TokenPoweredRequest>()
        if (!tokenInfo.isTherapist) {
            transaction(db.db) {
                DatabaseConnectHowIsLife.UserAccessTokens.deleteWhere { DatabaseConnectHowIsLife.UserAccessTokens.access_token eq tokenInfo.accessToken }
            }
        } else {
            transaction(db.db) {
                DatabaseConnectHowIsLife.TherapistAccessTokens.deleteWhere { DatabaseConnectHowIsLife.TherapistAccessTokens.access_token eq tokenInfo.accessToken }
            }
        }
        println("deleted: ${tokenInfo.accessToken}")
        call.respondText("OK")
    }

    post("how-is-life/add_qrcode_auth") {
        val tokenInfo = call.receive<TokenPoweredRequest>()
        transaction(db.db) {
            DatabaseConnectHowIsLife.UserAccessTokens.deleteWhere { DatabaseConnectHowIsLife.UserAccessTokens.access_token eq tokenInfo.accessToken }
        }
        println("deleted: ${tokenInfo.accessToken}")
        call.respondText("OK")
    }

    post("how-is-life/submit_daily_app_usage") {
        try {
            val data = call.receive<AppUsageStatisticDay>()
            val userID = verifyUserInternalAccessToken(data.access_token)
            // unauthenticated after 90 day inactivity
            if (userID == -2L) {
                call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                return@post
            }
            if (userID == -1L) {
                call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                return@post
            }

            transaction(db.db) {
                val result = DatabaseConnectHowIsLife.Users.select {
                    DatabaseConnectHowIsLife.Users.id eq userID
                }.firstOrNull()
                if (result != null) {
                    val submittedDataIterator = data.usageStatsForDay.listIterator()
                    val usageStatsRaw: DatabaseConnectHowIsLife.AppUsageStatisticsDaily =
                            DatabaseConnectHowIsLife.AppUsageStatisticsDaily.fromString(result[DatabaseConnectHowIsLife.Users.app_usage_statistics])
                            ?: DatabaseConnectHowIsLife.AppUsageStatisticsDaily()
                    val usageStats = usageStatsRaw.listIterator()
                    while (usageStats.hasNext()) {
                        val oldAppUsageInfo = usageStats.next()

                        while (submittedDataIterator.hasNext()) {
                            val newAppUsageInfo = submittedDataIterator.next()

                            if (newAppUsageInfo.pkg_name == oldAppUsageInfo.pkg_name) {
                                val rawBeforeAverageMinutes = oldAppUsageInfo.usage_minutes * oldAppUsageInfo.days_recorded
                                oldAppUsageInfo.days_recorded++
                                val newAverage = (rawBeforeAverageMinutes + newAppUsageInfo.usage_minutes) / oldAppUsageInfo.days_recorded
                                oldAppUsageInfo.usage_minutes = newAverage
                                usageStats.set(oldAppUsageInfo)

                                submittedDataIterator.remove()
                                break
                            }
                        }
                    }
                    for (newUniqueData in data.usageStatsForDay) {
                        newUniqueData.days_recorded = 1
                        usageStats.add(newUniqueData)
                    }
                    println("result usageStats: ${usageStatsRaw}")
                    DatabaseConnectHowIsLife.Users.update({ DatabaseConnectHowIsLife.Users.id eq userID }) {
                        it[DatabaseConnectHowIsLife.Users.app_usage_statistics] = usageStatsRaw.toString()
                    }
                } else {
                    println("submit_daily_app_usage -> user not found: $userID")
                }
            }
            call.respondText("OK")
        } catch (exc: Throwable) {
            //exc.printStackTrace()
            errorHappened(exc)
        }
    }

    post("how-is-life/change_data") {
        try {
            val data = JSONObject(call.receiveText())
            val isTherapist = data.optBoolean("isTherapist")

            if (isTherapist) {
                changeTherapistData(data, call)
            } else {
                changeUserData(data, call)
            }
        } catch (exc: Throwable) {
            //exc.printStackTrace()
            errorHappened(exc)
        }
    }

    post("how-is-life/change_profile_pic") {
        try {
            val data = JSONObject(call.receiveText())
            val isTherapist = data.optBoolean("isTherapist")

            val accessToken = data.optString("accessToken")

            val newProfilePic = data.optString("newProfilePic")
            val newProfilePicSmall = data.optString("newProfilePicSmall")
            if (newProfilePic.isBlank() || newProfilePicSmall.isBlank()) {
                call.respondText("Empty pic!", status = HttpStatusCode.BadRequest)
                return@post
            }

            val bytes = Base64.getDecoder().decode(newProfilePic)
            val bytesSmall = Base64.getDecoder().decode(newProfilePicSmall)

            if (!isTherapist) {
                val userID = verifyUserInternalAccessToken(accessToken)
                // unauthenticated after 90 day inactivity
                if (userID == -2L) {
                    call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                    return@post
                }
                if (userID == -1L) {
                    call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                    return@post
                }

                transaction(db.db) {
                    DatabaseConnectHowIsLife.Users.update({ DatabaseConnectHowIsLife.Users.id eq userID }) {
                        it[DatabaseConnectHowIsLife.Users.profile_pic] = SerialBlob(bytes)
                        it[DatabaseConnectHowIsLife.Users.small_profile_pic] = SerialBlob(bytesSmall)
                    }
                }
            } else {
                val therapistID = verifyTherapistInternalAccessToken(accessToken)
                // unauthenticated after 90 day inactivity
                if (therapistID == -2L) {
                    call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
                    return@post
                }
                if (therapistID == -1L) {
                    call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
                    return@post
                }

                transaction(db.db) {
                    DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq therapistID }) {
                        it[DatabaseConnectHowIsLife.Therapists.profile_pic] = SerialBlob(bytes)
                        it[DatabaseConnectHowIsLife.Users.small_profile_pic] = SerialBlob(bytesSmall)
                    }
                }
            }

            call.respondText("OK")
        } catch (exc: Throwable) {
            //exc.printStackTrace()
            errorHappened(exc)
        }
    }

    post("how-is-life/profile_pic") {
        try {
            val params = call.receive<GetData>()
            if (!params.isTherapist) {
                val userID = verifyUserInternalAccessToken(params.accessToken)
                // unauthenticated after 90 day inactivity
                if (userID == -2L) {
                    return@post
                }
                if (userID == -1L) {
                    return@post
                }

                var bytes: ByteArray? = null

                if (params.therapistID != null) {
                    transaction(db.db) {
                        val resultRow = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq params.therapistID }.firstOrNull()
                        if (resultRow != null) {
                            val picBlob = resultRow[DatabaseConnectHowIsLife.Therapists.profile_pic]
                            bytes = picBlob?.binaryStream?.readBytes()
                        }
                    }
                } else {
                    transaction(db.db) {
                        val resultRow = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.id eq userID }.firstOrNull()
                        if (resultRow != null) {
                            val picBlob = resultRow[DatabaseConnectHowIsLife.Users.profile_pic]
                            bytes = picBlob?.binaryStream?.readBytes()
                        }
                    }
                }
                if (bytes != null) {
                    call.respondText(Base64.getEncoder().encodeToString(bytes!!))
                }
            } else {
                val therapistID = verifyTherapistInternalAccessToken(params.accessToken)
                // unauthenticated after 90 day inactivity
                if (therapistID == -2L) {
                    return@post
                }
                if (therapistID == -1L) {
                    return@post
                }

                var bytes: ByteArray? = null

                if (params.userID != null) {
                    transaction(db.db) {
                        val permissions = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
                            (DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id eq therapistID) and (DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq params.userID) }.firstOrNull()

                        if (permissions?.get(DatabaseConnectHowIsLife.TherapistUserPermissions.profile_picture) == DatabaseConnectHowIsLife.PermissionStatus.Granted) {
                            val resultRow = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.id eq params.userID }.firstOrNull()
                            if (resultRow != null) {
                                val picBlob = resultRow[DatabaseConnectHowIsLife.Users.profile_pic]
                                bytes = picBlob?.binaryStream?.readBytes()
                            }
                        }
                    }
                } else {
                    transaction(db.db) {
                        val resultRow = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq therapistID }.firstOrNull()
                        if (resultRow != null) {
                            val picBlob = resultRow[DatabaseConnectHowIsLife.Therapists.profile_pic]
                            bytes = picBlob?.binaryStream?.readBytes()
                        }
                    }
                }
                if (bytes != null) {
                    call.respondText(Base64.getEncoder().encodeToString(bytes!!))
                }
            }
        } catch (exc: Throwable) {
            //exc.printStackTrace()
            errorHappened(exc)
        }
    }

    post("how-is-life/is_therapist_requested") {
        val data = call.receive<GetData>()
        val userID = verifyUserInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.therapistID == null) {
            call.respondText("No therapistID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        var requested = false

        transaction(db.db) {
            val therapist = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq data.therapistID }.firstOrNull()
            if (therapist != null) {
                val userIdRequestsRaw = (therapist[DatabaseConnectHowIsLife.Therapists.user_requests] ?: "").split(",")
                if (userIdRequestsRaw.contains(data.userID.toString())) {
                    requested = true
                }
            }
        }

        call.respondText(if (requested) "1" else "0", status = HttpStatusCode.OK)
    }

    post("how-is-life/therapist_request/{action}") {
        val data = call.receive<GetData>()
        val userID = verifyUserInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.therapistID == null) {
            call.respondText("No therapistID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        val action = call.parameters["action"]

        var out = "Failed connection to Database!"
        var statusCode = HttpStatusCode.InternalServerError

        transaction(db.db) {
            val therapist = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq data.therapistID }.firstOrNull()
            if (therapist != null) {
                val userIdRequestsRaw = therapist[DatabaseConnectHowIsLife.Therapists.user_requests] ?: ""
                val userIdRequests = userIdRequestsRaw.split(",").toMutableList()
                when(action) {
                    "check" -> {
                        out = if (userIdRequests.contains(userID.toString())) "1" else "0"
                        statusCode = HttpStatusCode.OK
                    }
                    "add" -> {
                        if (userIdRequests.size > 200) {
                            out = "More than 200 pending requests to this therapist. Failure!"
                            statusCode = HttpStatusCode.BadRequest
                        } else {
                            DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq data.therapistID }) {
                                it[DatabaseConnectHowIsLife.Therapists.user_requests] = if (userIdRequestsRaw.isEmpty()) "$userID" else "$userIdRequestsRaw,$userID"
                            }
                            out = "OK"
                            statusCode = HttpStatusCode.OK
                        }
                    }
                    "cancel" -> {
                        if (!userIdRequests.remove(userID.toString())) {
                            out = "Failed!"
                            statusCode = HttpStatusCode.BadRequest
                        } else {
                            DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq data.therapistID }) {
                                it[DatabaseConnectHowIsLife.Therapists.user_requests] = userIdRequests.joinToString(",")
                            }
                            out = "OK"
                            statusCode = HttpStatusCode.OK
                        }
                    }
                }
            }
        }

        call.respondText(out, status = statusCode)
    }
    
    //TODO: send out notifications

    post("how-is-life/request_permission") {
        val data = call.receive<PermissionAction>()
        val therapistID = verifyTherapistInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (therapistID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (therapistID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.userID == null) {
            call.respondText("No userID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        transaction(db.db) {
            val  result = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
                (DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id eq therapistID) and (DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq data.userID)
            }.firstOrNull()

            if (result == null) {
                DatabaseConnectHowIsLife.TherapistUserPermissions.insert {
                    it[therapist_id] = therapistID
                    it[user_id] = data.userID
                    for (permission in data.permissions) {
                        val action = DatabaseConnectHowIsLife.PermissionStatus.values()[permission.action]
                        it[getPermissionRow(DatabaseConnectHowIsLife.UserToTherapistPermissions.values()[permission.permission])] = if (action == DatabaseConnectHowIsLife.PermissionStatus.Granted) DatabaseConnectHowIsLife.PermissionStatus.Requested else action
                    }
                }
            } else {
                DatabaseConnectHowIsLife.TherapistUserPermissions
                        .update({DatabaseConnectHowIsLife.TherapistUserPermissions.id eq result[DatabaseConnectHowIsLife.TherapistUserPermissions.id]}) {
                            for (permission in data.permissions) {
                                val row = getPermissionRow(DatabaseConnectHowIsLife.UserToTherapistPermissions.values()[permission.permission])
                                val action = DatabaseConnectHowIsLife.PermissionStatus.values()[permission.action]
                                if (result[row] != DatabaseConnectHowIsLife.PermissionStatus.Granted) {
                                    it[row] = if (action == DatabaseConnectHowIsLife.PermissionStatus.Granted) DatabaseConnectHowIsLife.PermissionStatus.Requested else action
                                }
                            }
                }
            }
        }

        call.respondText("OK", status = HttpStatusCode.OK)
    }

    post("how-is-life/list_permissions") {
        val data = call.receive<GetData>()
        val therapistID = verifyTherapistInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (therapistID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (therapistID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.userID == null) {
            call.respondText("No userID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        val permissions = ArrayList<Permission>()

        transaction(db.db) {
            val  result = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
                (DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id eq therapistID) and (DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq data.userID)
            }.firstOrNull()

            for (permission in DatabaseConnectHowIsLife.UserToTherapistPermissions.values()) {
                if (result != null) {
                    val row = getPermissionRow(permission)
                    permissions.add(Permission(permission.ordinal, result[row].ordinal))
                } else {
                    permissions.add(Permission(permission.ordinal, DatabaseConnectHowIsLife.PermissionStatus.NotGranted.ordinal))
                }
            }
        }

        call.respondText(objectMapper.writeValueAsString(permissions), status = HttpStatusCode.OK)
    }

    post("how-is-life/set_permission_state") {
        val data = call.receive<PermissionAction>()
        val userID = verifyUserInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.therapistID == null) {
            call.respondText("No therapistID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        var errorNotFound = false

        transaction(db.db) {
            val  result = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
                (DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id eq data.therapistID) and (DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq userID)
            }.firstOrNull()

            if (result == null) {
                errorNotFound = true
            } else {
                DatabaseConnectHowIsLife.TherapistUserPermissions
                        .update({DatabaseConnectHowIsLife.TherapistUserPermissions.id eq result[DatabaseConnectHowIsLife.TherapistUserPermissions.id]}) {
                            for (permission in data.permissions) {
                                val row = getPermissionRow(DatabaseConnectHowIsLife.UserToTherapistPermissions.values()[permission.permission])
                                it[row] = DatabaseConnectHowIsLife.PermissionStatus.values()[permission.action]
                            }
                        }
            }
        }
        if (errorNotFound) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }
        call.respondText("OK", status = HttpStatusCode.OK)
    }

    /*
        TODO: search by language - sort from first to last of user's languages
        1. User's 1. language with therapist's 1. and 2. language
        2. User's 2. language with therapist's 1. and 2. language
        3. User's 3. language with therapist's 1. and 2. language
        4. User's other languages as 1., 2. therapist language
        5. User's other languages as 3. and more therapist language
        6. Whatever the fuck is left
     */
    post("how-is-life/find_therapists") {
        val data = call.receive<FilterData>()
        val userID = verifyUserInternalAccessToken(data.access_token)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

       val statement = DatabaseConnectHowIsLife.Therapists.select {
            (DatabaseConnectHowIsLife.Therapists.id greater data.last_id)
        }.limit(50).groupBy(DatabaseConnectHowIsLife.Therapists.id)
       if (data.name.isNotBlank()) {
           val splittedName = data.name.split(" ")
           var orNameExpression: Op<Boolean>? = null
           for (name in splittedName) {
               if (name.isNotBlank()) {
                   val expr = Op.build { DatabaseConnectHowIsLife.Therapists.username like "%$name%" }
                   orNameExpression = if (orNameExpression == null) expr else (orNameExpression or expr)
               }
           }
           if (orNameExpression != null) {
               statement.andWhere { orNameExpression }
           }
       }
       if (data.profilePic) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.profile_pic.isNotNull() }
       }
       if (data.gender != DatabaseConnectHowIsLife.Gender.Unspecified.ordinal) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.gender eq DatabaseConnectHowIsLife.Gender.values()[data.gender] }
       }
       if (data.sex != DatabaseConnectHowIsLife.Sex.Unspecified.ordinal) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.sex eq DatabaseConnectHowIsLife.Sex.values()[data.sex] }
       }
       if (data.summary) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.summary.isNotNull() }
       }
       if (data.resume) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.resume.isNotNull() }
       }
       if (data.professional) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.is_professional eq true }
       }
       if (data.externalCommunication) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.external_communications.isNotNull() }
       }
       if (data.verified) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.is_verified eq true }
       }
       var checkGenderOptions = true
       val genderOptions = DatabaseConnectHowIsLife.GenderOptions()
       when (data.transOption) {
           "want_to_transition" -> {
               genderOptions.want_to_transition = true
           }
           "transitioning" -> {
               genderOptions.transitioning = true
           }
           "has_transitioned" -> {
               genderOptions.has_transitioned = true
           }
           else -> {
               checkGenderOptions = false
           }
       }
       when (data.demiOption) {
           "leaning_to_male" -> {
               genderOptions.leaning_to_male = true
               checkGenderOptions = true
           }
           "leaning_to_female" -> {
               genderOptions.leaning_to_female = true
               checkGenderOptions = true
           }
       }

       if (checkGenderOptions) {
           statement.andWhere { DatabaseConnectHowIsLife.Therapists.gender_options eq genderOptions.toString() }
       }

       var out: Pair<ArrayList<TherapistSearchResult>, Long>? = null
       transaction(db.db) {
           var bytes: ByteArray?
           var picBlob: Blob?
           val outList = ArrayList<TherapistSearchResult>()
           var lastID = 0L
           statement.forEach {
               lastID = it[DatabaseConnectHowIsLife.Therapists.id]
               picBlob = it[DatabaseConnectHowIsLife.Therapists.small_profile_pic]
               bytes = picBlob?.binaryStream?.readBytes()

               outList.add(TherapistSearchResult(lastID,
                       if (bytes != null) Base64.getEncoder().encodeToString(bytes!!) else null,
                       it[DatabaseConnectHowIsLife.Therapists.username],
                       it[DatabaseConnectHowIsLife.Therapists.age],
                       it[DatabaseConnectHowIsLife.Therapists.is_professional],
                       it[DatabaseConnectHowIsLife.Therapists.is_verified]))

               picBlob?.free()
           }
           out = Pair(outList, lastID)
       }
       if (out != null) {
           call.respondText(objectMapper.writeValueAsString(out!!), status = HttpStatusCode.OK)
       } else {
           call.respondText("Error", status = HttpStatusCode.InternalServerError)
       }
    }

    /**
     * @see GetData.userID - the startIN
     */
    post("how-is-life/user_requests/{action}") {
        val data = call.receive<GetData>()
        val therapistID = verifyTherapistInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (therapistID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (therapistID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.userID == null) {
            call.respondText("No userID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        val action = call.parameters["action"]

        var out: Pair<ArrayList<UserSearchResult>, Long>? = null
        var limit = -1

        var delete = false
        var accept = false

        var otherActionFailed = false

        when(action) {
            "few" -> limit = 3
            "all" -> limit = 20
            "delete" -> delete = true
            "accept" -> accept = true
        }
        transaction(db.db) {
            val therapist = DatabaseConnectHowIsLife.Therapists.select {
                (DatabaseConnectHowIsLife.Therapists.id eq therapistID)
            }.firstOrNull()

            if (therapist != null) {

                val userIdRequestsRaw = therapist[DatabaseConnectHowIsLife.Therapists.user_requests] ?: ""
                val userIdRequests = userIdRequestsRaw.split(",")


                if (!delete && !accept) {
                    var startIndex = userIdRequests.indexOf(data.userID.toString())
                    if (startIndex == -1) {
                        startIndex = 0
                    }
                    val endIndex = (startIndex + limit).coerceAtMost(userIdRequests.size)

                    println("Start: $startIndex End: $endIndex")

                    val limitedList = userIdRequests.subList(startIndex, endIndex)

                    var bytes: ByteArray?
                    var picBlob: Blob?
                    val outList = ArrayList<UserSearchResult>()
                    var lastID = 0L

                    var requestingUsers: Query? = DatabaseConnectHowIsLife.Users.selectAll()
                    for (userID in limitedList) {
                        if (userID.isEmpty()) {
                            requestingUsers = null
                            break
                        }
                        requestingUsers!!.adjustWhere {
                            val expr = Op.build { DatabaseConnectHowIsLife.Users.id eq userID.toLong() }
                            if (this == null) expr
                            else this or expr
                        }
                    }
                    requestingUsers?.forEach {
                        lastID = it[DatabaseConnectHowIsLife.Users.id]
                        picBlob = it[DatabaseConnectHowIsLife.Users.small_profile_pic]
                        bytes = picBlob?.binaryStream?.readBytes()

                        outList.add(UserSearchResult(lastID,
                                if (bytes != null) Base64.getEncoder().encodeToString(bytes!!) else null,
                                it[DatabaseConnectHowIsLife.Users.username],
                                it[DatabaseConnectHowIsLife.Users.age],
                                it[DatabaseConnectHowIsLife.Users.summary] != null,
                                it[DatabaseConnectHowIsLife.Users.job]))

                        picBlob?.free()
                    }

                    out = Pair(outList, lastID)
                } else if (delete) {
                    val mutableUserIdRequests = userIdRequests.toMutableList()
                    if (!mutableUserIdRequests.remove(data.userID.toString())) {
                        deleteFailed = true
                    } else {
                        DatabaseConnectHowIsLife.Therapists.update({ DatabaseConnectHowIsLife.Therapists.id eq therapistID }) {
                            it[DatabaseConnectHowIsLife.Therapists.user_requests] = mutableUserIdRequests.joinToString(",")
                        }
                    }
                } else if (accept) {

                }
            }
        }
        if (!delete) {
            if (out != null) {
                call.respondText(objectMapper.writeValueAsString(out!!), status = HttpStatusCode.OK)
            } else {
                call.respondText("Error", status = HttpStatusCode.InternalServerError)
            }
        } else {
            if (otherActionFailed) {
                call.respondText("Failed!", status = HttpStatusCode.BadRequest)
            } else {
                call.respondText("OK", status = HttpStatusCode.OK)
            }
        }
    }

    post("how-is-life/therapist_permission_requests/{action}") {
        val data = call.receive<GetData>()
        val userID = verifyUserInternalAccessToken(data.accessToken)
        // unauthenticated after 90 day inactivity
        if (userID == -2L) {
            call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
            return@post
        }
        if (userID == -1L) {
            call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
            return@post
        }

        if (data.therapistID == null) {
            call.respondText("No therapistID provided!", status = HttpStatusCode.BadRequest)
            return@post
        }

        val action = call.parameters["action"]

        var out: Pair<ArrayList<BasicTherapistInfo>, Long>? = null
        var limit = -1
        var lastTherapistID = 0L

        when(action) {
            "few" -> limit = 3
            "all" -> limit = 20
        }
        transaction(db.db) {
            val therapistUserPermissions = DatabaseConnectHowIsLife.TherapistUserPermissions.select {
                (DatabaseConnectHowIsLife.TherapistUserPermissions.user_id eq userID) and
                        (DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id greater data.therapistID)
            }.limit(limit)

            val therapistPermissionMap = ArrayList<BasicTherapistInfo>()
            therapistUserPermissions.forEach {
                val permissionsPerTherapist = ArrayList<Permission>()
                for (permission in DatabaseConnectHowIsLife.UserToTherapistPermissions.values()) {
                    val row  = getPermissionRow(permission)
                    val permissionStatus = it[row]
                    if (permissionStatus == DatabaseConnectHowIsLife.PermissionStatus.Requested) {
                        permissionsPerTherapist.add(Permission(permission.ordinal, permissionStatus.ordinal))
                    }
                }
                if (permissionsPerTherapist.isNotEmpty()) {
                    lastTherapistID = it[DatabaseConnectHowIsLife.TherapistUserPermissions.therapist_id]
                    val therapistUsername = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq lastTherapistID }.firstOrNull()
                    therapistPermissionMap.add(BasicTherapistInfo(lastTherapistID, therapistUsername?.get(DatabaseConnectHowIsLife.Therapists.username) ?: "Unknown", permissionsPerTherapist))
                }
            }
            out = Pair(therapistPermissionMap, lastTherapistID)
        }
        if (out != null) {
            call.respondText(objectMapper.writeValueAsString(out!!), status = HttpStatusCode.OK)
        } else {
            call.respondText("Error", status = HttpStatusCode.InternalServerError)
        }
    }

    webSocket("how-is-life/real_time_boxes") {
        //manage those goddamn sockets
        var userID: Long = -1
        var accessToken: String = ""
        var doneWithToken = false
        try {
            for (frame in incoming) {
                when (frame) {
                    is Frame.Text -> {
                        try {
                            val text = frame.readText()
                            //you get the same custID as from the form
                            if (text.startsWith("id:") && !doneWithToken) {
                                doneWithToken = true
                                val custId = text.substring(3)
                                userID = verifyInternalUserAccessTokenCareAboutValidity(custId)
                                System.out.println("userID: $userID")
                                // unauthenticated after 90 day inactivity
                                if (userID == -2L) {
                                    close(reason = CloseReason(CloseReason.Codes.VIOLATED_POLICY, "Authentication failed!"))
                                    return@webSocket
                                }
                                if (userID == -1L) {
                                    close(reason = CloseReason(CloseReason.Codes.TRY_AGAIN_LATER, "Error occurred!"))
                                    return@webSocket
                                }
                                if (userID == -3L) {
                                    accessToken = custId
                                    var currentList = connectedWaitingVerificationWebsocketUsers[accessToken]
                                    if (currentList == null) {
                                        currentList = mutableListOf()
                                    }
                                    currentList.add(this)
                                    connectedWaitingVerificationWebsocketUsers[accessToken] = currentList
                                } else {
                                    var currentList = connectedMoodboxWebsocketUsers[userID]
                                    if (currentList == null) {
                                        currentList = mutableListOf()
                                    }
                                    currentList.add(this)
                                    connectedMoodboxWebsocketUsers[userID] = currentList
                                }
                            }
                            if (text.equals("bye", ignoreCase = true)) {
                                close(reason = CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                            }
                        } catch (exc: Throwable) {
                            errorHappened(exc, call)
                        }
                    }
                }
            }
        } catch (exc: Throwable) {
            errorHappened(exc)
        } finally {
            if (userID != -1L) {
                System.out.println("userID: $userID disconnected!")
                val currentList = connectedMoodboxWebsocketUsers[userID]
                if (currentList != null) {
                    currentList.remove(this)
                    connectedMoodboxWebsocketUsers[userID] = currentList
                }
            } else if (accessToken.isNotEmpty()) {
                System.out.println("accessToken: $accessToken disconnected!")
                val currentList = connectedWaitingVerificationWebsocketUsers[accessToken]
                if (currentList != null) {
                    currentList.remove(this)
                    connectedWaitingVerificationWebsocketUsers[accessToken] = currentList
                }
            }
        }
    }
}

fun getUserData(userID: Long): UserData? {
    var userData: UserData? = null
    transaction(db.db) {
        val itemUser = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.id eq userID }.firstOrNull()
        if (itemUser != null) {
            val profilePicBlob = itemUser[DatabaseConnectHowIsLife.Users.profile_pic]
            profilePicBlob?.free()
            userData = UserData(
                    itemUser[DatabaseConnectHowIsLife.Users.username],
                    itemUser[DatabaseConnectHowIsLife.Users.registration_type].ordinal,
                    itemUser[DatabaseConnectHowIsLife.Users.email],
                    profilePicBlob != null,
                    itemUser[DatabaseConnectHowIsLife.Users.gender]?.ordinal,
                    itemUser[DatabaseConnectHowIsLife.Users.sex]?.ordinal,
                    itemUser[DatabaseConnectHowIsLife.Users.gender_options],
                    itemUser[DatabaseConnectHowIsLife.Users.summary],
                    itemUser[DatabaseConnectHowIsLife.Users.resume],
                    itemUser[DatabaseConnectHowIsLife.Users.age],
                    itemUser[DatabaseConnectHowIsLife.Users.job],
                    itemUser[DatabaseConnectHowIsLife.Users.job_stress_level]?.ordinal,
                    itemUser[DatabaseConnectHowIsLife.Users.app_usage_statistics],
                    itemUser[DatabaseConnectHowIsLife.Users.external_communications],
                    itemUser[DatabaseConnectHowIsLife.Users.spoken_languages],
                    itemUser[DatabaseConnectHowIsLife.Users.can_be_found_by_therapists]
            )
        }
    }
    return userData
}

fun getTherapistData(therapistID: Long, hideMail: Boolean): TherapistData? {
    var therapistData: TherapistData? = null
    transaction(db.db) {
        val itemTherapist = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq therapistID }.firstOrNull()
        if (itemTherapist != null) {
            val profilePicBlob = itemTherapist[DatabaseConnectHowIsLife.Therapists.profile_pic]
            profilePicBlob?.free()
            therapistData = TherapistData(
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.username],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.registration_type].ordinal,
                    if (hideMail) "" else itemTherapist[DatabaseConnectHowIsLife.Therapists.email],
                    profilePicBlob != null,
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.gender]?.ordinal,
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.sex]?.ordinal,
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.gender_options],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.summary],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.resume],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.age],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.job],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.external_communications],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.is_professional],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.is_verified],
                    itemTherapist[DatabaseConnectHowIsLife.Therapists.spoken_languages]
            )
        }
    }
    return therapistData
}

suspend fun changeTherapistData(data: JSONObject, call: ApplicationCall) {
    val therapistID = verifyTherapistInternalAccessToken(data.optString("accessToken"))
    // unauthenticated after 90 day inactivity
    if (therapistID == -2L) {
        call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
        return
    }
    if (therapistID == -1L) {
        call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
        return
    }

    val newVal = data.opt("newVal")
    val action = data.optString("action")

    var statement: (DatabaseConnectHowIsLife.Therapists.(UpdateStatement)->Unit)? = null

    when (action) {
        "change_sex" -> {
            statement = {
                it[sex] = if (newVal != JSONObject.NULL) DatabaseConnectHowIsLife.Sex.values()[newVal as Int] else null
            }
        }
        "change_gender" -> {
            statement = {
                it[gender] = if (newVal != JSONObject.NULL) DatabaseConnectHowIsLife.Gender.values()[newVal as Int] else null
            }
        }
        "change_transition" -> {
            var genderOptions: DatabaseConnectHowIsLife.GenderOptions = DatabaseConnectHowIsLife.GenderOptions()
            transaction(db.db) {
                genderOptions = DatabaseConnectHowIsLife.GenderOptions.fromString(DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq therapistID }.firstOrNull()?.get(DatabaseConnectHowIsLife.Therapists.gender_options)) ?: genderOptions
            }
            genderOptions.nullTransitionStatus()
            val ok = if (newVal != JSONObject.NULL) newVal else ""
            when (ok as String) {
                "want_to_transition" -> {
                    genderOptions.want_to_transition = true
                }
                "transitioning" -> {
                    genderOptions.transitioning = true
                }
                "has_transitioned" -> {
                    genderOptions.has_transitioned = true
                }
            }
            statement = {
                it[gender_options] = genderOptions.toString()
            }
        }
        "change_demi" -> {
            var genderOptions: DatabaseConnectHowIsLife.GenderOptions = DatabaseConnectHowIsLife.GenderOptions()
            transaction(db.db) {
                genderOptions = DatabaseConnectHowIsLife.GenderOptions.fromString(DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.id eq therapistID }.firstOrNull()?.get(DatabaseConnectHowIsLife.Therapists.gender_options)) ?: genderOptions
            }
            genderOptions.nullDemiStatus()
            val ok = if (newVal != JSONObject.NULL) newVal else ""
            when (ok as String) {
                "leaning_to_male" -> {
                    genderOptions.leaning_to_male = true
                }
                "leaning_to_female" -> {
                    genderOptions.leaning_to_female = true
                }
            }
            statement = {
                it[gender_options] = genderOptions.toString()
            }
        }
        "change_languages" -> {
            statement = {
                it[spoken_languages] = newVal as String
            }
        }
    }

    if (statement == null && newVal == JSONObject.NULL) {
        call.respondText("Empty value!", status = HttpStatusCode.BadRequest)
        return
    }

    when(action) {
        "change_professional_status" -> {
            statement = {
                it[is_professional] = newVal as Boolean
            }
        }

        "change_username" -> {
            statement = {
                it[username] = newVal as String
            }
        }
        "change_external_communication" -> {
            statement = {
                it[external_communications] = newVal as String
            }
        }
        "change_summary" -> {
            statement = {
                it[summary] = newVal as String
            }
        }
        "change_resume" -> {
            statement = {
                it[resume] = newVal as String
            }
        }
        "change_age" -> {
            statement = {
                it[age] = newVal as Int
            }
        }
        "change_job" -> {
            statement = {
                it[job] = newVal as String
            }
        }
    }
    if (statement == null) {
        call.respondText("Database error!", status = HttpStatusCode.InternalServerError)
        return
    }
    transaction(db.db) {
        DatabaseConnectHowIsLife.Therapists.update({DatabaseConnectHowIsLife.Therapists.id eq therapistID}, body = statement!!)
    }

    call.respondText("OK")
}

suspend fun changeUserData(data: JSONObject, call: ApplicationCall) {
    val userID = verifyUserInternalAccessToken(data.optString("accessToken"))
    // unauthenticated after 90 day inactivity
    if (userID == -2L) {
        call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
        return
    }
    if (userID == -1L) {
        call.respondText("Error occurred!\nCouldn't sync to cloud.", status = HttpStatusCode.InternalServerError)
        return
    }

    val newVal = data.opt("newVal")
    val action = data.optString("action")

    var statement: (DatabaseConnectHowIsLife.Users.(UpdateStatement)->Unit)? = null

    when (action) {
        "change_sex" -> {
            statement = {
                it[sex] = if (newVal != JSONObject.NULL) DatabaseConnectHowIsLife.Sex.values()[newVal as Int] else null
            }
        }
        "change_gender" -> {
            statement = {
                it[gender] = if (newVal != JSONObject.NULL) DatabaseConnectHowIsLife.Gender.values()[newVal as Int] else null
            }
        }
        "change_job_stress_level" -> {
            statement = {
                it[job_stress_level] = if (newVal != JSONObject.NULL) DatabaseConnectHowIsLife.JobStressLevel.values()[newVal as Int] else null
            }
        }
        "change_transition" -> {
            var genderOptions: DatabaseConnectHowIsLife.GenderOptions = DatabaseConnectHowIsLife.GenderOptions()
            transaction(db.db) {
                genderOptions = DatabaseConnectHowIsLife.GenderOptions.fromString(DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.id eq userID }.firstOrNull()?.get(DatabaseConnectHowIsLife.Users.gender_options)) ?: genderOptions
            }
            genderOptions.nullTransitionStatus()
            val ok = if (newVal != JSONObject.NULL) newVal else ""
            when (ok as String) {
                "want_to_transition" -> {
                    genderOptions.want_to_transition = true
                }
                "transitioning" -> {
                    genderOptions.transitioning = true
                }
                "has_transitioned" -> {
                    genderOptions.has_transitioned = true
                }
            }
            statement = {
                it[gender_options] = genderOptions.toString()
            }
        }
        "change_demi" -> {
            var genderOptions: DatabaseConnectHowIsLife.GenderOptions = DatabaseConnectHowIsLife.GenderOptions()
            transaction(db.db) {
                genderOptions = DatabaseConnectHowIsLife.GenderOptions.fromString(DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.id eq userID }.firstOrNull()?.get(DatabaseConnectHowIsLife.Users.gender_options)) ?: genderOptions
            }
            genderOptions.nullDemiStatus()
            val ok = if (newVal != JSONObject.NULL) newVal else ""
            when (ok as String) {
                "leaning_to_male" -> {
                    genderOptions.leaning_to_male = true
                }
                "leaning_to_female" -> {
                    genderOptions.leaning_to_female = true
                }
            }
            statement = {
                it[gender_options] = genderOptions.toString()
            }
        }
        "change_languages" -> {
            statement = {
                it[spoken_languages] = newVal as String
            }
        }
        "change_therapist_can_find" -> {
            statement = {
                it[can_be_found_by_therapists] = newVal as Boolean
            }
        }
    }

    if (statement == null && newVal == JSONObject.NULL) {
        call.respondText("Empty value!", status = HttpStatusCode.BadRequest)
        return
    }

    when(action) {
        "change_username" -> {
            statement = {
                it[username] = newVal as String
            }
        }
        "change_external_communication" -> {
            statement = {
                it[external_communications] = newVal as String
            }
        }
        "change_summary" -> {
            statement = {
                it[summary] = newVal as String
            }
        }
        "change_resume" -> {
            statement = {
                it[resume] = newVal as String
            }
        }
        "change_age" -> {
            statement = {
                it[age] = newVal as Int
            }
        }
        "change_job" -> {
            statement = {
                it[job] = newVal as String
            }
        }
    }
    if (statement == null) {
        call.respondText("Database error!", status = HttpStatusCode.InternalServerError)
        return
    }
    transaction(db.db) {
        DatabaseConnectHowIsLife.Users.update({DatabaseConnectHowIsLife.Users.id eq userID}, body = statement!!)
    }

    call.respondText("OK", status = HttpStatusCode.OK)
}

fun handleRegisterPassword(regInfo: RegisterInfo,
                           call: ApplicationCall,
                           registerTherapist: Boolean = false): Pair<Long, String> {
    var accessToken = ""
    var userId: Long = -1
    // TODO: check if Email is unique
    if (!registerTherapist) {
        transaction(db.db) {
            userId = DatabaseConnectHowIsLife.Users.insert {
                it[email] = regInfo.email
                it[password] = BCrypt.hashpw(regInfo.password, BCrypt.gensalt())
                it[username] = regInfo.username
                it[date_of_registration] = System.currentTimeMillis()
            }[DatabaseConnectHowIsLife.Users.id]
            accessToken = generateAccessToken(220)
            DatabaseConnectHowIsLife.UserAccessTokens.insert {
                it[user_id] = userId
                it[access_token] = accessToken
                it[last_used] = System.currentTimeMillis()
            }
            DatabaseConnectHowIsLife.UserLogins.insert {
                it[user_id] = userId
                it[access_token] = accessToken
                it[device] = regInfo.device
                it[date_time] = System.currentTimeMillis()
                it[ip_address] = call.request.origin.remoteHost
            }
        }
    } else {
        transaction(db.db) {
            userId = DatabaseConnectHowIsLife.Therapists.insert {
                it[email] = regInfo.email
                it[password] = BCrypt.hashpw(regInfo.password, BCrypt.gensalt())
                it[username] = regInfo.username
                it[date_of_registration] = System.currentTimeMillis()
            }[DatabaseConnectHowIsLife.Therapists.id]
            accessToken = generateAccessToken(220)
            DatabaseConnectHowIsLife.TherapistAccessTokens.insert {
                it[therapist_id] = userId
                it[access_token] = accessToken
                it[last_used] = System.currentTimeMillis()
            }
            DatabaseConnectHowIsLife.TherapistLogins.insert {
                it[therapist_id] = userId
                it[access_token] = accessToken
                it[device] = regInfo.device
                it[date_time] = System.currentTimeMillis()
                it[ip_address] = call.request.origin.remoteHost
            }
        }
    }
    return Pair(userId, accessToken)
}

fun generateAccessToken(length: Int): String {

    val allAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789".toCharArray()

    //Use cryptographically secure random number generator
    val random = SecureRandom()

    val password = StringBuilder(length)

    for (i in 0 until length) {
        password.append(allAllowed[random.nextInt(allAllowed.size)])
    }

    return password.toString()

}

fun getVerifiedFBid(userToken: String): String? {
    println("token: $userToken")
    var retries = 5
    var hasError = true
    while (retries >= 0 && hasError) {
        retries--
        try {
            var link =
                    "https://graph.facebook.com/debug_token?input_token=$userToken&access_token=$clientId|$clientSecret"
            var url = URL(link)
            var connection = url.openConnection() as HttpsURLConnection

            var responseCode = connection.responseCode
            if (responseCode == 200) {
                val FBResponse = connection.inputStream.use { it.readBytes() }.toString(Charsets.UTF_8)
                println(FBResponse)

                val json = JSONObject(FBResponse)
                //root object
                val data = json.getJSONObject("data")
                if (data.optString("app_id") == clientId &&
                        data.optBoolean("is_valid") &&
                        data.optString("user_id").isNotBlank()) {
                    return data.optString("user_id")
                }
            } else {
                println("Invalid ID token.")
                hasError = true
            }
        } catch (exc: Throwable) {
            hasError = true
            exc.printStackTrace()
        }
    }
    return null
}

// returns First as UserID, Second as AccessToken
suspend fun handleRegisterFacebook(regInfo: RegisterInfo,
                                   registrationType: DatabaseConnectHowIsLife.RegistrationType,
                                   call: ApplicationCall,
                                   registerTherapist: Boolean = false): Pair<Long, String> {
    println("token: ${regInfo.password}")
    var idOut: Long = -1
    var retries = 5
    var hasError = true
    var accessToken = ""
    while (retries >= 0 && hasError) {
        retries--
        try {
            val FBUserId = getVerifiedFBid(regInfo.password)
            if (FBUserId != null) {
                val providedEmail = regInfo.email
                val facebookAccess = "https://graph.facebook.com/me?fields=name,first_name,last_name${if (/*providedEmail.isEmpty()*/true) ",email" else ""}&access_token=${regInfo.password}"
                val url = URL(facebookAccess)
                val connection = url.openConnection() as HttpsURLConnection

                val responseCode = connection.responseCode
                if (responseCode == 200) {
                    val FBMeResponse = connection.inputStream.use { it.readBytes() }.toString(Charsets.UTF_8)
                    println(FBMeResponse)

                    val jsonMe = JSONObject(FBMeResponse)
                    val secondFBuserId = jsonMe.optString("id")
                    if (secondFBuserId == FBUserId) {
                        val email = jsonMe.optString("email")
                        val mailToUse = if (email.isNullOrBlank()) providedEmail else email
                        if (!registerTherapist) {
                            transaction(db.db) {
                                val emailFound: ResultRow? = if (email.isNullOrBlank()) DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.email eq email }.firstOrNull() else null
                                val loginID3rdPartyFound = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.login_id_3rd_party eq secondFBuserId }.firstOrNull()
                                when {
                                    loginID3rdPartyFound != null -> {
                                        idOut = loginID3rdPartyFound[DatabaseConnectHowIsLife.Users.id]
                                        if (loginID3rdPartyFound[DatabaseConnectHowIsLife.Users.email].isEmpty()) {
                                            DatabaseConnectHowIsLife.Users.update({DatabaseConnectHowIsLife.Users.id eq idOut}) {
                                                it[DatabaseConnectHowIsLife.Users.email] = mailToUse
                                            }
                                        }
                                    }
                                    emailFound == null -> idOut = DatabaseConnectHowIsLife.Users.insert {
                                        it[DatabaseConnectHowIsLife.Users.email] = mailToUse
                                        it[login_id_3rd_party] = secondFBuserId
                                        it[username] = jsonMe.optString("name")
                                        it[registration_type] = registrationType
                                        it[date_of_registration] = System.currentTimeMillis()
                                    }[DatabaseConnectHowIsLife.Users.id]

                                    else -> idOut = emailFound[DatabaseConnectHowIsLife.Users.id]
                                }
                                accessToken = generateAccessToken(220)
                                DatabaseConnectHowIsLife.UserAccessTokens.insert {
                                    it[user_id] = idOut
                                    it[access_token] = accessToken
                                    it[last_used] = System.currentTimeMillis()
                                }
                                DatabaseConnectHowIsLife.UserLogins.insert {
                                    it[user_id] = idOut
                                    it[access_token] = accessToken
                                    it[device] = regInfo.device
                                    it[date_time] = System.currentTimeMillis()
                                    it[registration_type] = registrationType
                                    it[ip_address] = call.request.origin.remoteHost
                                }
                            }
                        } else {
                            transaction(db.db) {
                                val emailFound: ResultRow? = if (email.isNullOrBlank()) DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.email eq email }.firstOrNull() else null
                                val loginID3rdPartyFound = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.login_id_3rd_party eq secondFBuserId }.firstOrNull()
                                when {
                                    loginID3rdPartyFound != null -> {
                                        idOut = loginID3rdPartyFound[DatabaseConnectHowIsLife.Therapists.id]
                                        if (loginID3rdPartyFound[DatabaseConnectHowIsLife.Therapists.email].isEmpty()) {
                                            DatabaseConnectHowIsLife.Therapists.update({DatabaseConnectHowIsLife.Therapists.id eq idOut}) {
                                                it[DatabaseConnectHowIsLife.Therapists.email] = mailToUse
                                            }
                                        }
                                    }
                                    emailFound == null -> idOut = DatabaseConnectHowIsLife.Therapists.insert {
                                        it[DatabaseConnectHowIsLife.Therapists.email] = mailToUse
                                        it[login_id_3rd_party] = secondFBuserId
                                        it[username] = jsonMe.optString("name")
                                        it[registration_type] = registrationType
                                        it[date_of_registration] = System.currentTimeMillis()
                                    }[DatabaseConnectHowIsLife.Therapists.id]

                                    else -> idOut = emailFound[DatabaseConnectHowIsLife.Therapists.id]
                                }
                                accessToken = generateAccessToken(220)
                                DatabaseConnectHowIsLife.TherapistAccessTokens.insert {
                                    it[therapist_id] = idOut
                                    it[access_token] = accessToken
                                    it[last_used] = System.currentTimeMillis()
                                }
                                DatabaseConnectHowIsLife.TherapistLogins.insert {
                                    it[therapist_id] = idOut
                                    it[access_token] = accessToken
                                    it[device] = regInfo.device
                                    it[date_time] = System.currentTimeMillis()
                                    it[registration_type] = registrationType
                                    it[ip_address] = call.request.origin.remoteHost
                                }
                            }
                        }
                        hasError = false
                    }
                }
            }
        } catch (exc: Throwable) {
            hasError = true
            exc.printStackTrace()
        }
    }
    if (hasError) {
        call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
    }
    return Pair(idOut, accessToken)
}

// returns First as UserID, Second as AccessToken
suspend fun handleRegisterGoogle(regInfo: RegisterInfo,
                                 registrationType: DatabaseConnectHowIsLife.RegistrationType,
                                 call: ApplicationCall,
                                 registerTherapist: Boolean = false): Pair<Long, String> {
    println("token: ${regInfo.password}")
    var idOut: Long = -1
    var retries = 5
    var hasError = true
    var accessToken = ""
    while (retries >= 0 && hasError) {
        retries--
        try {
            val idToken: GoogleIdToken? = verifier.verify(regInfo.password)
            if (idToken != null) {
                val payload = idToken.payload

                print(payload.toPrettyString())

                // Print user identifier
                val userId = payload.subject
                println("User ID: $userId")

                // Get profile information from payload
                val emailGoogle = payload.email
                val name = (payload["name"] as String?).orEmpty().ifEmpty { emailGoogle.substring(startIndex = 0, endIndex = emailGoogle.indexOf('@')) }
                println("name: $name")

                /*
                val emailVerified = java.lang.Boolean.valueOf(payload.emailVerified)
                val pictureUrl = payload["picture"] as String
                val locale = payload["locale"] as String
                val familyName = payload["family_name"] as String
                val givenName = payload["given_name"] as String
                */

                if (!registerTherapist) {
                    transaction(db.db) {
                        val emailQuery = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.email eq emailGoogle }.firstOrNull()
                        val loginID3rdPartyFound = DatabaseConnectHowIsLife.Users.select { DatabaseConnectHowIsLife.Users.login_id_3rd_party eq userId }.firstOrNull()
                        when {
                            loginID3rdPartyFound != null -> {
                                idOut = loginID3rdPartyFound[DatabaseConnectHowIsLife.Users.id]
                                if (loginID3rdPartyFound[DatabaseConnectHowIsLife.Users.email].isEmpty()) {
                                    DatabaseConnectHowIsLife.Users.update({DatabaseConnectHowIsLife.Users.id eq idOut}) {
                                        it[DatabaseConnectHowIsLife.Users.email] = emailGoogle
                                    }
                                }
                            }
                            emailQuery == null -> idOut = DatabaseConnectHowIsLife.Users.insert {
                                it[email] = emailGoogle
                                it[login_id_3rd_party] = userId
                                it[username] = name
                                it[registration_type] = registrationType
                                it[date_of_registration] = System.currentTimeMillis()
                            }[DatabaseConnectHowIsLife.Users.id]

                            else -> idOut = emailQuery[DatabaseConnectHowIsLife.Users.id]
                        }
                        accessToken = generateAccessToken(220)
                        DatabaseConnectHowIsLife.UserAccessTokens.insert {
                            it[user_id] = idOut
                            it[access_token] = accessToken
                            it[last_used] = System.currentTimeMillis()
                        }
                        DatabaseConnectHowIsLife.UserLogins.insert {
                            it[user_id] = idOut
                            it[access_token] = accessToken
                            it[device] = regInfo.device
                            it[date_time] = System.currentTimeMillis()
                            it[registration_type] = registrationType
                            it[ip_address] = call.request.origin.remoteHost
                        }
                    }
                } else {
                    transaction(db.db) {
                        val emailQuery = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.email eq emailGoogle }.firstOrNull()
                        val loginID3rdPartyFound = DatabaseConnectHowIsLife.Therapists.select { DatabaseConnectHowIsLife.Therapists.login_id_3rd_party eq userId }.firstOrNull()
                        when {
                            loginID3rdPartyFound != null -> {
                                idOut = loginID3rdPartyFound[DatabaseConnectHowIsLife.Therapists.id]
                                if (loginID3rdPartyFound[DatabaseConnectHowIsLife.Therapists.email].isEmpty()) {
                                    DatabaseConnectHowIsLife.Therapists.update({DatabaseConnectHowIsLife.Therapists.id eq idOut}) {
                                        it[DatabaseConnectHowIsLife.Therapists.email] = emailGoogle
                                    }
                                }
                            }
                            emailQuery == null -> idOut = DatabaseConnectHowIsLife.Therapists.insert {
                                it[email] = emailGoogle
                                it[login_id_3rd_party] = userId
                                it[username] = name
                                it[registration_type] = registrationType
                                it[date_of_registration] = System.currentTimeMillis()
                            }[DatabaseConnectHowIsLife.Therapists.id]

                            else -> idOut = emailQuery[DatabaseConnectHowIsLife.Therapists.id]
                        }
                        accessToken = generateAccessToken(220)
                        DatabaseConnectHowIsLife.TherapistAccessTokens.insert {
                            it[therapist_id] = idOut
                            it[access_token] = accessToken
                            it[last_used] = System.currentTimeMillis()
                        }
                        DatabaseConnectHowIsLife.TherapistLogins.insert {
                            it[therapist_id] = idOut
                            it[access_token] = accessToken
                            it[device] = regInfo.device
                            it[date_time] = System.currentTimeMillis()
                            it[registration_type] = registrationType
                            it[ip_address] = call.request.origin.remoteHost
                        }
                    }
                }
            } else {
                println("Invalid ID token.")
            }
            hasError = false
        } catch (exc: Throwable) {
            hasError = true
            exc.printStackTrace()
        }
    }
    if (hasError) {
        call.respondText("Authentication failed!", status = HttpStatusCode.Unauthorized)
    }
    return Pair(idOut, accessToken)
}
