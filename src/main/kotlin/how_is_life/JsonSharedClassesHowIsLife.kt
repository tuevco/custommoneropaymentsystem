package how_is_life

data class RegisterInfo(var email: String = "",
                        var username: String = "",
                        var password: String = "",
                        var device: String = "",
                        var signInMethod: String = "",
                        var dataSoFar: String = "",
                        var isTherapist: Boolean = false)

data class LoginInfo(val email: String,
                     val password: String,
                     val device: String,
                     val dataSoFar: String,
                     val signInMethod: String)

data class UserData(val username: String,
                    val login_type: Int,
                    val email: String,
                    val profile_pic: Boolean,
                    val gender: Int?,
                    val sex: Int?,
                    val gender_options: String?, // no need to reencode a json
                    val summary: String?,
                    val resume: String?,
                    val age: Int?,
                    val job: String?,
                    val job_stress_level: Int?,
                    val app_usage_statistics: String?, // no need to reencode a json
                    val external_communications: String?,
                    val spoken_languages: String?,
                    val can_be_found_by_therapists: Boolean)

data class TherapistData(val username: String,
                         val login_type: Int,
                         val email: String,
                         val profile_pic: Boolean,
                         val gender: Int?,
                         val sex: Int?,
                         val gender_options: String?, // no need to reencode a json
                         val summary: String?,
                         val resume: String?,
                         val age: Int?,
                         val job: String?,
                         val external_communications: String?,
                         val professional: Boolean,
                         val verified: Boolean,
                         val spoken_languages: String?)

data class AppUsageStatisticDay(val access_token: String,
                                val usageStatsForDay: ArrayList<DatabaseConnectHowIsLife.BasicDailyAppUsageStatistic>)

data class TokenPoweredRequest(val accessToken: String,
                               val signInMethod: String?,
                               val isTherapist: Boolean)

data class GetData(val accessToken: String,
                   val isTherapist: Boolean,
                   val therapistID: Long?,
                   val userID: Long?)

data class Permission(val permission: Int,
                      val action: Int)

data class PermissionAction(val accessToken: String,
                            val isTherapist: Boolean,
                            val userID: Long?,
                            val therapistID: Long?,
                            val permissions: ArrayList<Permission>)

data class LoginInfoExists(val email: String,
                           val access_token: String,
                           val signInMethod: String)

data class VerifyInvalidToken(val validAccessToken: String,
                              val tokenToBeValidated: String)

data class FilterData(val last_id: Long,
                      val access_token: String,
                      val name: String,
                      val profilePic: Boolean,
                      val gender: Int,
                      val sex: Int,
                      val transOption: String?,
                      val demiOption: String?,
                      val summary: Boolean,
                      val resume: Boolean,
                      val age_start: Int,
                      val age_end: Int,
                      val professional: Boolean,
                      val externalCommunication: Boolean,
                      val verified: Boolean)

data class TherapistSearchResult(val therapistID: Long,
                                 val profile_pic: String?,
                                 val name: String,
                                 val age: Int?,
                                 val professional: Boolean,
                                 val verified: Boolean)

data class UserSearchResult(val userID: Long,
                            val profile_pic: String?,
                            val name: String,
                            val age: Int?,
                            val summary: Boolean,
                            val job: String?)

data class BasicTherapistInfo(val therapistID: Long,
                              val username: String,
                              val requestedPermissions: ArrayList<Permission>)

fun getRegistrationTypeFromString(signInMethod: String): DatabaseConnectHowIsLife.RegistrationType {
    return when(signInMethod) {
        "password" -> DatabaseConnectHowIsLife.RegistrationType.Password
        "facebook" -> DatabaseConnectHowIsLife.RegistrationType.Facebook
        "google" -> DatabaseConnectHowIsLife.RegistrationType.Google
        "kakao" -> DatabaseConnectHowIsLife.RegistrationType.Kakao
        "line" -> DatabaseConnectHowIsLife.RegistrationType.Line
        "wechat" -> DatabaseConnectHowIsLife.RegistrationType.WeChat
        else -> DatabaseConnectHowIsLife.RegistrationType.Password
    }
}