package redditparse;

import com.fasterxml.jackson.annotation.JsonProperty;

//the thing I will receive in the data payload (fcm notification)
public class SendToAdmin {
    private final String title;
    private final String url;
    private final String note;
    private final String redditLink;

    public SendToAdmin(String title, String url, String note, String redditLink) {
        this.title = title;
        this.url = url;
        this.note = note;
        this.redditLink = redditLink;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("reddit")
    public String getRedditLink() {
        return redditLink;
    }
}

