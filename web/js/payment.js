/**
 * @return {string}
 */
var ID = function () {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    return '_' + Math.random().toString(36).substr(2, 9);
};
var parentFormify = function (children) {
    return "<div id=\"formParent\">"+children+"</div>";
};
$(document).ready(function() {
    var id = ID();
    $("#custId").val(id);
    var product = $("#product");
    if (product.val().includes("source") || true) {
        $.getJSON("https://ip.seeip.org/jsonip?callback=?",
            function (json) {
                $("#ip").val(json.ip);
            }
        );
    }
    var productString = product.val();
    $("#payment-form").submit(function (e) {
        $('#formSubmitBtn').attr('disabled', true);
        e.preventDefault();
        var paymentdata = $("#payment-form").serialize();
        console.log("data: "+paymentdata);
        $.ajax({
            type: "POST",
            url: "../qrcode",
            data: paymentdata, // serializes the form's elements.
            success: function (data) {
                $("#formParent").replaceWith(parentFormify(data));
                var full = location.hostname+(location.port ? ':'+location.port: '');
                var socket = new WebSocket(location.protocol.replace("http", "ws")+"//" + full + "/paymentConnector");
                socket.onerror = function () {
                    console.log("socket error");
                };

                socket.onopen = function (ws) {
                    console.log("Connected");
                    socket.send("id:" + id);
                    if (!Notification) {
                        return;
                    }

                    if (Notification.permission !== "granted")
                        Notification.requestPermission();
                };

                socket.onclose = function (evt) {
                    var explanation = "";
                    if (evt.reason && evt.reason.length > 0) {
                        explanation = "reason: " + evt.reason;
                    } else {
                        explanation = "without a reason specified";
                    }

                    console.log("Disconnected with close code " + evt.code + " and " + explanation);
                };

                socket.onmessage = function (event) {
                    if (event.data.toString() === "Transaction completed!") {
                        var download = productString !== "binarykt" ? "Link is sent to your email!" : "<a href='../blog.html'>See how to implement</a>";
                        $("#formParent").replaceWith(parentFormify("<h1>Everything is completed, feel free to leave the page!<br>"+download+"</h1>"));
                    } else {
                        $("#formParent").replaceWith(parentFormify("<h3>"+event.data.toString()+"</h3>"));
                    }

                    if (Notification.permission !== "granted") {
                        alert(event.data.toString());
                    } else {
                        var notification = new Notification('Transaction info', {
                            icon: '/android-chrome-192x192.png',
                            body: event.data.toString()
                        });

                        notification.onclick = function () {

                        };

                    }
                };
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
        return false;
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('.tooltip-wrapper').tooltip({position: "bottom"});
});
function recaptchaCallbackForm() {
    $('#formSubmitBtn').removeAttr('disabled');
    var tooltipWrap = $('.tooltip-wrapper');
    tooltipWrap.removeClass('tooltip-wrapper disabled');
    tooltipWrap.tooltip('disable');
}