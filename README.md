# Custom fast monero payment system with Java/Kotlin Server and a sample JavaScript client

I have made this as a part of another bigger project and I am working towards generalising it and making it an embeddable library. 

This payment system is part of the How is Life Mood Tracker app.